<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241006085741 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE commit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE merge_request_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE patch_note_section_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE repository_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE submodule_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE submodule_version_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE working_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE working_item_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE client (id INT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE commit (id INT NOT NULL, version_id UUID NOT NULL, code VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4ED42EAD4BBC2705 ON commit (version_id)');
        $this->addSql('CREATE UNIQUE INDEX commit_unique_code ON commit (code)');
        $this->addSql('CREATE UNIQUE INDEX commit_unique_link ON commit (link)');
        $this->addSql('COMMENT ON COLUMN commit.version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE instance (id UUID NOT NULL, repository_id INT NOT NULL, client_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4230B1DE50C9D4F7 ON instance (repository_id)');
        $this->addSql('CREATE INDEX IDX_4230B1DE19EB6921 ON instance (client_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_name_per_repository ON instance (name, repository_id)');
        $this->addSql('COMMENT ON COLUMN instance.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE instance_version (instance_id UUID NOT NULL, version_id UUID NOT NULL, PRIMARY KEY(instance_id, version_id))');
        $this->addSql('CREATE INDEX IDX_78881E003A51721D ON instance_version (instance_id)');
        $this->addSql('CREATE INDEX IDX_78881E004BBC2705 ON instance_version (version_id)');
        $this->addSql('COMMENT ON COLUMN instance_version.instance_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN instance_version.version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE merge_request (id INT NOT NULL, version_id UUID NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(150) NOT NULL, link VARCHAR(255) NOT NULL, merged_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, processed_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, closed_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, source_branch VARCHAR(100) NOT NULL, target_branch VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5F2666E14BBC2705 ON merge_request (version_id)');
        $this->addSql('CREATE UNIQUE INDEX merge_request_unique_code ON merge_request (code)');
        $this->addSql('CREATE UNIQUE INDEX merge_request_unique_link ON merge_request (link)');
        $this->addSql('COMMENT ON COLUMN merge_request.version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN merge_request.merged_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN merge_request.processed_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN merge_request.closed_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE merge_request_merge_request (merge_request_source INT NOT NULL, merge_request_target INT NOT NULL, PRIMARY KEY(merge_request_source, merge_request_target))');
        $this->addSql('CREATE INDEX IDX_D08BF50B2C1C44 ON merge_request_merge_request (merge_request_source)');
        $this->addSql('CREATE INDEX IDX_D08BF50B19C94CCB ON merge_request_merge_request (merge_request_target)');
        $this->addSql('CREATE TABLE merge_request_version (merge_request_id INT NOT NULL, version_id UUID NOT NULL, PRIMARY KEY(merge_request_id, version_id))');
        $this->addSql('CREATE INDEX IDX_7FED0AF2FCB3624 ON merge_request_version (merge_request_id)');
        $this->addSql('CREATE INDEX IDX_7FED0AF4BBC2705 ON merge_request_version (version_id)');
        $this->addSql('COMMENT ON COLUMN merge_request_version.version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE merge_request_working_item (merge_request_id INT NOT NULL, working_item_id INT NOT NULL, PRIMARY KEY(merge_request_id, working_item_id))');
        $this->addSql('CREATE INDEX IDX_896C82382FCB3624 ON merge_request_working_item (merge_request_id)');
        $this->addSql('CREATE INDEX IDX_896C8238CC1FF01E ON merge_request_working_item (working_item_id)');
        $this->addSql('CREATE TABLE patch_note_section (id INT NOT NULL, merge_request_id INT DEFAULT NULL, version_id UUID DEFAULT NULL, commit_id INT DEFAULT NULL, code VARCHAR(15) NOT NULL, name VARCHAR(30) NOT NULL, content JSON NOT NULL, required BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_445A1C0D2FCB3624 ON patch_note_section (merge_request_id)');
        $this->addSql('CREATE INDEX IDX_445A1C0D4BBC2705 ON patch_note_section (version_id)');
        $this->addSql('CREATE INDEX IDX_445A1C0D3D5814AC ON patch_note_section (commit_id)');
        $this->addSql('COMMENT ON COLUMN patch_note_section.version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE repository (id INT NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(50) NOT NULL, link VARCHAR(255) NOT NULL, token VARCHAR(255) DEFAULT NULL, main_branch VARCHAR(10) NOT NULL, dev_branch VARCHAR(10) NOT NULL, host VARCHAR(30) NOT NULL, based_code_identifier VARCHAR(10) NOT NULL, create_branch_for_new_version BOOLEAN DEFAULT true NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX repository_unique_code ON repository (code, based_code_identifier)');
        $this->addSql('CREATE UNIQUE INDEX repository_unique_name ON repository (name, based_code_identifier)');
        $this->addSql('CREATE TABLE submodule (id INT NOT NULL, parent_id INT NOT NULL, child_id INT NOT NULL, code VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, path VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6A58AE0D727ACA70 ON submodule (parent_id)');
        $this->addSql('CREATE INDEX IDX_6A58AE0DDD62C21B ON submodule (child_id)');
        $this->addSql('CREATE TABLE submodule_branch_version (id INT NOT NULL, branch VARCHAR(100) NOT NULL, pull_latest_changes BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE submodule_existing_version (id INT NOT NULL, target_version_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D98981007950FF94 ON submodule_existing_version (target_version_id)');
        $this->addSql('COMMENT ON COLUMN submodule_existing_version.target_version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE submodule_new_version (id INT NOT NULL, submodule_version_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_64C86FF5DC53FF05 ON submodule_new_version (submodule_version_id)');
        $this->addSql('COMMENT ON COLUMN submodule_new_version.submodule_version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE submodule_version (id INT NOT NULL, parent_version_id UUID NOT NULL, submodule_id INT NOT NULL, update_type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_749A6A2ACFFA355 ON submodule_version (parent_version_id)');
        $this->addSql('CREATE INDEX IDX_749A6A2A3BAD1EA8 ON submodule_version (submodule_id)');
        $this->addSql('COMMENT ON COLUMN submodule_version.parent_version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, uid UUID NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_IDENTIFIER_EMAIL ON "user" (email)');
        $this->addSql('COMMENT ON COLUMN "user".uid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE version (id UUID NOT NULL, repository_id INT NOT NULL, code VARCHAR(10) DEFAULT NULL, versioned_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, version_type VARCHAR(255) NOT NULL, release_version BOOLEAN NOT NULL, commit_sha VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BF1CD3C350C9D4F7 ON version (repository_id)');
        $this->addSql('CREATE UNIQUE INDEX version_unique_code_per_repository ON version (code, repository_id)');
        $this->addSql('COMMENT ON COLUMN version.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN version.versioned_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE worker (id INT NOT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE working_item (id INT NOT NULL, client_id INT DEFAULT NULL, parent_item_id INT DEFAULT NULL, type_id INT NOT NULL, support_by_id INT DEFAULT NULL, escalated_by_id INT DEFAULT NULL, planned_version_id UUID DEFAULT NULL, actual_version_id UUID DEFAULT NULL, realized_by_id INT DEFAULT NULL, uuid UUID NOT NULL, name VARCHAR(50) NOT NULL, description TEXT DEFAULT NULL, source VARCHAR(50) DEFAULT NULL, link VARCHAR(255) NOT NULL, priority_score INT DEFAULT NULL, supported_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, escalated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, planned_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, versioned_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, realized_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_416197E419EB6921 ON working_item (client_id)');
        $this->addSql('CREATE INDEX IDX_416197E460272618 ON working_item (parent_item_id)');
        $this->addSql('CREATE INDEX IDX_416197E4C54C8C93 ON working_item (type_id)');
        $this->addSql('CREATE INDEX IDX_416197E4585D36DC ON working_item (support_by_id)');
        $this->addSql('CREATE INDEX IDX_416197E48FE1AA9C ON working_item (escalated_by_id)');
        $this->addSql('CREATE INDEX IDX_416197E44C54A7D8 ON working_item (planned_version_id)');
        $this->addSql('CREATE INDEX IDX_416197E4728472A1 ON working_item (actual_version_id)');
        $this->addSql('CREATE INDEX IDX_416197E41ECF8E19 ON working_item (realized_by_id)');
        $this->addSql('COMMENT ON COLUMN working_item.planned_version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN working_item.actual_version_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN working_item.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN working_item.supported_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN working_item.escalated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN working_item.planned_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN working_item.versioned_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN working_item.realized_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE working_item_type (id INT NOT NULL, code VARCHAR(20) NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455BF396750 FOREIGN KEY (id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EAD4BBC2705 FOREIGN KEY (version_id) REFERENCES version (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE instance ADD CONSTRAINT FK_4230B1DE50C9D4F7 FOREIGN KEY (repository_id) REFERENCES repository (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE instance ADD CONSTRAINT FK_4230B1DE19EB6921 FOREIGN KEY (client_id) REFERENCES client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE instance_version ADD CONSTRAINT FK_78881E003A51721D FOREIGN KEY (instance_id) REFERENCES instance (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE instance_version ADD CONSTRAINT FK_78881E004BBC2705 FOREIGN KEY (version_id) REFERENCES version (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merge_request ADD CONSTRAINT FK_5F2666E14BBC2705 FOREIGN KEY (version_id) REFERENCES version (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merge_request_merge_request ADD CONSTRAINT FK_D08BF50B2C1C44 FOREIGN KEY (merge_request_source) REFERENCES merge_request (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merge_request_merge_request ADD CONSTRAINT FK_D08BF50B19C94CCB FOREIGN KEY (merge_request_target) REFERENCES merge_request (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merge_request_version ADD CONSTRAINT FK_7FED0AF2FCB3624 FOREIGN KEY (merge_request_id) REFERENCES merge_request (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merge_request_version ADD CONSTRAINT FK_7FED0AF4BBC2705 FOREIGN KEY (version_id) REFERENCES version (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merge_request_working_item ADD CONSTRAINT FK_896C82382FCB3624 FOREIGN KEY (merge_request_id) REFERENCES merge_request (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE merge_request_working_item ADD CONSTRAINT FK_896C8238CC1FF01E FOREIGN KEY (working_item_id) REFERENCES working_item (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE patch_note_section ADD CONSTRAINT FK_445A1C0D2FCB3624 FOREIGN KEY (merge_request_id) REFERENCES merge_request (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE patch_note_section ADD CONSTRAINT FK_445A1C0D4BBC2705 FOREIGN KEY (version_id) REFERENCES version (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE patch_note_section ADD CONSTRAINT FK_445A1C0D3D5814AC FOREIGN KEY (commit_id) REFERENCES commit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submodule ADD CONSTRAINT FK_6A58AE0D727ACA70 FOREIGN KEY (parent_id) REFERENCES repository (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submodule ADD CONSTRAINT FK_6A58AE0DDD62C21B FOREIGN KEY (child_id) REFERENCES repository (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submodule_branch_version ADD CONSTRAINT FK_90FF9A81BF396750 FOREIGN KEY (id) REFERENCES submodule_version (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submodule_existing_version ADD CONSTRAINT FK_D98981007950FF94 FOREIGN KEY (target_version_id) REFERENCES version (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submodule_existing_version ADD CONSTRAINT FK_D9898100BF396750 FOREIGN KEY (id) REFERENCES submodule_version (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submodule_new_version ADD CONSTRAINT FK_64C86FF5DC53FF05 FOREIGN KEY (submodule_version_id) REFERENCES version (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submodule_new_version ADD CONSTRAINT FK_64C86FF5BF396750 FOREIGN KEY (id) REFERENCES submodule_version (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submodule_version ADD CONSTRAINT FK_749A6A2ACFFA355 FOREIGN KEY (parent_version_id) REFERENCES version (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submodule_version ADD CONSTRAINT FK_749A6A2A3BAD1EA8 FOREIGN KEY (submodule_id) REFERENCES repository (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE version ADD CONSTRAINT FK_BF1CD3C350C9D4F7 FOREIGN KEY (repository_id) REFERENCES repository (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE worker ADD CONSTRAINT FK_9FB2BF62BF396750 FOREIGN KEY (id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE working_item ADD CONSTRAINT FK_416197E419EB6921 FOREIGN KEY (client_id) REFERENCES client (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE working_item ADD CONSTRAINT FK_416197E460272618 FOREIGN KEY (parent_item_id) REFERENCES working_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE working_item ADD CONSTRAINT FK_416197E4C54C8C93 FOREIGN KEY (type_id) REFERENCES working_item_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE working_item ADD CONSTRAINT FK_416197E4585D36DC FOREIGN KEY (support_by_id) REFERENCES worker (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE working_item ADD CONSTRAINT FK_416197E48FE1AA9C FOREIGN KEY (escalated_by_id) REFERENCES worker (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE working_item ADD CONSTRAINT FK_416197E44C54A7D8 FOREIGN KEY (planned_version_id) REFERENCES version (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE working_item ADD CONSTRAINT FK_416197E4728472A1 FOREIGN KEY (actual_version_id) REFERENCES version (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE working_item ADD CONSTRAINT FK_416197E41ECF8E19 FOREIGN KEY (realized_by_id) REFERENCES worker (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE commit_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE merge_request_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE patch_note_section_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE repository_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE submodule_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE submodule_version_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE working_item_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE working_item_type_id_seq CASCADE');
        $this->addSql('ALTER TABLE client DROP CONSTRAINT FK_C7440455BF396750');
        $this->addSql('ALTER TABLE commit DROP CONSTRAINT FK_4ED42EAD4BBC2705');
        $this->addSql('ALTER TABLE instance DROP CONSTRAINT FK_4230B1DE50C9D4F7');
        $this->addSql('ALTER TABLE instance DROP CONSTRAINT FK_4230B1DE19EB6921');
        $this->addSql('ALTER TABLE instance_version DROP CONSTRAINT FK_78881E003A51721D');
        $this->addSql('ALTER TABLE instance_version DROP CONSTRAINT FK_78881E004BBC2705');
        $this->addSql('ALTER TABLE merge_request DROP CONSTRAINT FK_5F2666E14BBC2705');
        $this->addSql('ALTER TABLE merge_request_merge_request DROP CONSTRAINT FK_D08BF50B2C1C44');
        $this->addSql('ALTER TABLE merge_request_merge_request DROP CONSTRAINT FK_D08BF50B19C94CCB');
        $this->addSql('ALTER TABLE merge_request_version DROP CONSTRAINT FK_7FED0AF2FCB3624');
        $this->addSql('ALTER TABLE merge_request_version DROP CONSTRAINT FK_7FED0AF4BBC2705');
        $this->addSql('ALTER TABLE merge_request_working_item DROP CONSTRAINT FK_896C82382FCB3624');
        $this->addSql('ALTER TABLE merge_request_working_item DROP CONSTRAINT FK_896C8238CC1FF01E');
        $this->addSql('ALTER TABLE patch_note_section DROP CONSTRAINT FK_445A1C0D2FCB3624');
        $this->addSql('ALTER TABLE patch_note_section DROP CONSTRAINT FK_445A1C0D4BBC2705');
        $this->addSql('ALTER TABLE patch_note_section DROP CONSTRAINT FK_445A1C0D3D5814AC');
        $this->addSql('ALTER TABLE submodule DROP CONSTRAINT FK_6A58AE0D727ACA70');
        $this->addSql('ALTER TABLE submodule DROP CONSTRAINT FK_6A58AE0DDD62C21B');
        $this->addSql('ALTER TABLE submodule_branch_version DROP CONSTRAINT FK_90FF9A81BF396750');
        $this->addSql('ALTER TABLE submodule_existing_version DROP CONSTRAINT FK_D98981007950FF94');
        $this->addSql('ALTER TABLE submodule_existing_version DROP CONSTRAINT FK_D9898100BF396750');
        $this->addSql('ALTER TABLE submodule_new_version DROP CONSTRAINT FK_64C86FF5DC53FF05');
        $this->addSql('ALTER TABLE submodule_new_version DROP CONSTRAINT FK_64C86FF5BF396750');
        $this->addSql('ALTER TABLE submodule_version DROP CONSTRAINT FK_749A6A2ACFFA355');
        $this->addSql('ALTER TABLE submodule_version DROP CONSTRAINT FK_749A6A2A3BAD1EA8');
        $this->addSql('ALTER TABLE version DROP CONSTRAINT FK_BF1CD3C350C9D4F7');
        $this->addSql('ALTER TABLE worker DROP CONSTRAINT FK_9FB2BF62BF396750');
        $this->addSql('ALTER TABLE working_item DROP CONSTRAINT FK_416197E419EB6921');
        $this->addSql('ALTER TABLE working_item DROP CONSTRAINT FK_416197E460272618');
        $this->addSql('ALTER TABLE working_item DROP CONSTRAINT FK_416197E4C54C8C93');
        $this->addSql('ALTER TABLE working_item DROP CONSTRAINT FK_416197E4585D36DC');
        $this->addSql('ALTER TABLE working_item DROP CONSTRAINT FK_416197E48FE1AA9C');
        $this->addSql('ALTER TABLE working_item DROP CONSTRAINT FK_416197E44C54A7D8');
        $this->addSql('ALTER TABLE working_item DROP CONSTRAINT FK_416197E4728472A1');
        $this->addSql('ALTER TABLE working_item DROP CONSTRAINT FK_416197E41ECF8E19');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE commit');
        $this->addSql('DROP TABLE instance');
        $this->addSql('DROP TABLE instance_version');
        $this->addSql('DROP TABLE merge_request');
        $this->addSql('DROP TABLE merge_request_merge_request');
        $this->addSql('DROP TABLE merge_request_version');
        $this->addSql('DROP TABLE merge_request_working_item');
        $this->addSql('DROP TABLE patch_note_section');
        $this->addSql('DROP TABLE repository');
        $this->addSql('DROP TABLE submodule');
        $this->addSql('DROP TABLE submodule_branch_version');
        $this->addSql('DROP TABLE submodule_existing_version');
        $this->addSql('DROP TABLE submodule_new_version');
        $this->addSql('DROP TABLE submodule_version');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE version');
        $this->addSql('DROP TABLE worker');
        $this->addSql('DROP TABLE working_item');
        $this->addSql('DROP TABLE working_item_type');
    }
}
