<?php

namespace App\ApiResource;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\OpenApi\Model;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\Parameter;
use App\Controller\BasedCode\FindRepositoriesController;
use App\Controller\BasedCode\FindSubmodulesController;
use App\Controller\BasedCode\SyncMergeRequestFromWebhookController;
use App\Controller\Repository\RepositorySearchBranchController;
use App\DTO\Repository\Branch;
use App\Entity\MergeRequest;
use App\Service\BasedCode\Handler\BasedCodeHandlerInterface;
use App\State\BasedCode\BasedCodeProvider;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    operations: [],
    normalizationContext: [
        'groups' => [self::API_READ_GROUP],
    ],
    denormalizationContext: [
        'groups' => [self::API_WRITE_GROUP],
    ],
    provider: BasedCodeProvider::class,
)]
#[GetCollection(
    uriTemplate: '/based_codes/{id}/repositories',
    controller: FindRepositoriesController::class,
    openapi: new Model\Operation(
        parameters: [
            new Model\Parameter(
                name: 'token',
                in: 'query',
                description: 'Auth token for based code api',
                required: true,
                example: 'gzdat-U9kMRovmed9vXaeRkurQ4',
            ),
        ]
    ),
    name: 'get_repositories_in_based_code'
)]
#[GetCollection(
    uriTemplate: '/based_codes/{id}/repositories/{code}/branches',
    controller: RepositorySearchBranchController::class,
    openapi: new Operation(
        parameters: [
            new Parameter(
                name: 'search',
                in: 'query',
                description: 'Search params to find branch',
                required: true,
                example: 'main',
            ),
        ]
    ),
    normalizationContext: ['groups' => Branch::API_READ_GROUP],
    output: Branch::class,
    name: 'search_branches',
)]
#[Get(
    uriTemplate: '/based_codes/{id}/repositories/{code}/submodules',
    controller: FindSubmodulesController::class,
    name: 'sync_mr'
)]
#[Post(
    uriTemplate: '/based_codes/{id}/merge_requests',
    inputFormats: ['json' => ['application/json']],
    controller: SyncMergeRequestFromWebhookController::class,
    normalizationContext: [
        'groups' => MergeRequest::API_READ_GROUP,
    ],
    output: MergeRequest::class,
    name: 'sync_merge_request_controller'
)]
#[GetCollection]
class BasedCode
{
    const string API_READ_GROUP = self::class.':read';
    const string API_WRITE_GROUP = self::class.':write';

    private BasedCodeHandlerInterface $basedCodeHandler;

    #[Groups([self::API_READ_GROUP])]
    public function getId(): string
    {
        return $this->basedCodeHandler->getIdentifier();
    }

    #[Groups([self::API_READ_GROUP])]
    public function getName(): string
    {
        return $this->basedCodeHandler->getName();
    }

    public function getBasedCodeHandler(): BasedCodeHandlerInterface
    {
        return $this->basedCodeHandler;
    }

    public function setBasedCodeHandler(BasedCodeHandlerInterface $basedCodeHandler): static
    {
        $this->basedCodeHandler = $basedCodeHandler;

        return $this;
    }
}
