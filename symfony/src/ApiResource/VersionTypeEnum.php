<?php
declare(strict_types=1);

namespace App\ApiResource;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\DTO\Version\PreparedVersionDTO;
use App\Enum\ExportableEnumInterface;
use App\Enum\ExportableEnumTrait;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ApiResource(
    shortName: 'VersionType',
    normalizationContext: [
        'groups' => ExportableEnumInterface::API_READ_GROUP,
    ]
)]
#[GetCollection(provider: self::class.'::getCases')]
#[Get(provider: self::class.'::getCase')]
enum VersionTypeEnum: string implements ExportableEnumInterface
{
    use ExportableEnumTrait;

    case PATCH = 'PATCH';
    case MINOR = 'MINOR';
    case MAJOR = 'MAJOR';

    #[Groups([
        self::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    public function getLabel(): string
    {
        return match ($this) {
            self::PATCH => 'Corrective',
            self::MINOR => 'Mineure',
            self::MAJOR => 'Majeure',
        };
    }

    #[Groups([
        self::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    public function getShortLabel(): string
    {
        return match ($this) {
            self::PATCH => 'Corr.',
            self::MINOR => 'Min.',
            self::MAJOR => 'Maj.',
        };
    }
}
