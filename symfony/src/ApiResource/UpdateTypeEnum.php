<?php
declare(strict_types=1);

namespace App\ApiResource;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\DTO\Version\PreparedVersionDTO;
use App\Enum\ExportableEnumInterface;
use App\Enum\ExportableEnumTrait;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

#[ApiResource(
    shortName: 'UpdateType',
    normalizationContext: [
        'groups' => ExportableEnumInterface::API_READ_GROUP,
    ]
)]
#[GetCollection(provider: self::class.'::getCases')]
#[Get(provider: self::class.'::getCase')]
enum UpdateTypeEnum: string implements ExportableEnumInterface
{
    use ExportableEnumTrait;

    case BRANCH = 'BRANCH';
    case NEW_VERSION = 'NEW_VERSION';
    case EXISTING_VERSION = 'EXISTING_VERSION';

    #[Groups([
        self::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP
    ])]
    public function getLabel(): string
    {
        return match ($this) {
            self::BRANCH => 'Branche',
            self::EXISTING_VERSION => 'Version Existante',
            self::NEW_VERSION => 'Nouvelle Version',
        };
    }

    #[Groups([
        self::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP
    ])]
    public function getShortLabel(): string
    {
        return match ($this) {
            self::BRANCH => 'Branche',
            self::EXISTING_VERSION => 'Vers. Exist.',
            self::NEW_VERSION => 'Nouv. Vers.',
        };
    }
}
