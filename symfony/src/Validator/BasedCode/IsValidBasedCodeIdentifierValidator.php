<?php

namespace App\Validator\BasedCode;

use App\Service\BasedCode\BasedCodeRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class IsValidBasedCodeIdentifierValidator extends ConstraintValidator
{
    public function __construct(
        private readonly BasedCodeRepository $basedCodeRepository
    )
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        /* @var IsValidBasedCodeIdentifier $constraint */

        if (null === $value || '' === $value) {
            return;
        }

        if ($this->basedCodeRepository->findById($value)) {
            return;
        }

        $this->context->addViolation('L\'identifiant "' . $value . '" ne correspond à aucune base de code géré par l\'outil');
    }
}
