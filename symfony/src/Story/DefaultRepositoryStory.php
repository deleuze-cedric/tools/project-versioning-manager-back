<?php

namespace App\Story;

use App\Factory\RepositoryFactory;
use Zenstruck\Foundry\Story;

final class DefaultRepositoryStory extends Story
{
    public function build(): void
    {
        $submodule = RepositoryFactory::createOne([
            'basedCodeIdentifier' => 'gitlab',
            'code' => 56386219,
            'devBranch' => 'dev',
            'host' => 'gitlab.com',
            'link' => 'https://gitlab.com/deleuze-cedric/tools/project-versioning-manager-back',
            'mainBranch' => 'main',
            'name' => 'Project versioning manager - Back',
            'token' => null,
        ]);

        RepositoryFactory::createOne([
            'basedCodeIdentifier' => 'gitlab',
            'code' => 56368064,
            'devBranch' => 'dev',
            'host' => 'gitlab.com',
            'link' => 'https://gitlab.com/deleuze-cedric/tools/project-versioning-manager',
            'mainBranch' => 'main',
            'name' => 'Project versioning manager',
            'token' => null,
            'subModules' => [$submodule]
        ]);
    }
}
