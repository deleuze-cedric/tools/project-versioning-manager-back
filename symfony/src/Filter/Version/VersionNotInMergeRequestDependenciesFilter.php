<?php

namespace App\Filter\Version;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Entity\MergeRequest;
use App\Entity\Version;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Contracts\Service\Attribute\Required;

class VersionNotInMergeRequestDependenciesFilter extends AbstractFilter
{
    private readonly IriConverterInterface $iriConverter;

    #[Required]
    public function setIriConverter(IriConverterInterface $iriConverter): void {
        $this->iriConverter = $iriConverter;
    }

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        if (!$this->isPropertyEnabled($property, $resourceClass)) {
            return;
        }

        /** @var MergeRequest $mergeRequest */
        $mergeRequest = $this->iriConverter->getResourceFromIri($value);

        $versions = $mergeRequest->getVersionDependencies()->getValues();
        $versions[] = $mergeRequest->getVersion();

        $queryBuilder
            ->andWhere('o.id NOT IN (:versions)')
            ->setParameter('versions', $versions);
    }

    public function getDescription(string $resourceClass): array
    {
        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description[$property] = [
                'property' => $property,
                'type' => Type::BUILTIN_TYPE_ARRAY,
                'required' => false,
                'description' => 'Filter version not in mr dependencies',
                'openapi' => [
                    'example' => '/api/merge_requests/4',
                    'allowReserved' => false,
                    'allowEmptyValue' => true,
                    'explode' => false,
                ],
            ];
        }

        return $description;
    }
}