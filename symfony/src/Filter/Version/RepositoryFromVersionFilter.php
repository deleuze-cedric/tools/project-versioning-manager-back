<?php

namespace App\Filter\Version;

use ApiPlatform\Api\IriConverterInterface;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Contracts\Service\Attribute\Required;

class RepositoryFromVersionFilter extends AbstractFilter
{
    private readonly IriConverterInterface $iriConverter;

    #[Required]
    public function setIriConverter(IriConverterInterface $iriConverter): void {
        $this->iriConverter = $iriConverter;
    }

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        if (!$this->isPropertyEnabled($property, $resourceClass)) {
            return;
        }

        $repository = $this->iriConverter->getResourceFromIri($value);

        if (!$repository) {
            return;
        }

        $queryBuilder
            ->innerJoin('o.version', 'version')
            ->andWhere('version.repository = :repo')
            ->setParameter('repo', $repository);
    }

    // This function is only used to hook in documentation generators (supported by Swagger and Hydra)
    public function getDescription(string $resourceClass): array
    {
        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description[$property] = [
                'property' => $property,
                'type' => Type::BUILTIN_TYPE_RESOURCE,
                'required' => false,
                'description' => 'Filter by repository defined in version (iri)',
                'openapi' => [
                    'example' => '/api/repositories/1234',
                    'allowReserved' => false,
                    'allowEmptyValue' => true,
                    'explode' => false,
                ],
            ];
        }

        return $description;
    }
}