<?php

namespace App\Filter\Version;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\PropertyInfo\Type;

class VersionGlobalSearchFilter extends AbstractFilter
{
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        if (!$this->isPropertyEnabled($property, $resourceClass)) {
            return;
        }

        $queryBuilder
            ->innerJoin('o.repository', 'repository')
            ->andWhere(
                $queryBuilder->expr()->orX(
                    'o.code LIKE :value',
                    'o.code LIKE :value',
                    'repository.code LIKE :value',
                    'repository.name LIKE :value',
                    'repository.link LIKE :value',
                )
            )
            ->setParameter('value', '%'.$value.'%');
    }

    // This function is only used to hook in documentation generators (supported by Swagger and Hydra)
    public function getDescription(string $resourceClass): array
    {
        $description = [];
        foreach ($this->properties as $property => $strategy) {
            $description[$property] = [
                'property' => $property,
                'type' => Type::BUILTIN_TYPE_STRING,
                'required' => false,
                'description' => 'Filter by code, name or link in mr and repository',
                'openapi' => [
                    'example' => 'Merge request title',
                    'allowReserved' => false,
                    'allowEmptyValue' => true,
                    'explode' => false,
                ],
            ];
        }

        return $description;
    }
}