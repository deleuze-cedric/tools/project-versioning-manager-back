<?php
declare(strict_types=1);

namespace App\DTO\Version;

use App\ApiResource\UpdateTypeEnum;
use App\ApiResource\VersionTypeEnum;
use App\Entity\Repository;
use App\Entity\Version;
use Symfony\Component\Serializer\Attribute\Groups;

// TODO : rajouter les validations pour vérifier que toutes les dépendances sont correctes (mr vers versions, mr vers mr, ...)
class PreparedVersionDTO
{
    public const string API_WRITE_GROUP = self::class.':api:write';
    public const string API_READ_GROUP = self::class.':api:read';

    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    public UpdateTypeEnum $updateType;

    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    public Repository $repository;

    /**
     * @var iterable<PreparedVersionDTO>
     */
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    public iterable $submoduleReleases = [];

    // Branch

    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    public ?string $branch = null;

    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    public ?bool $pullLatestChanges = null;

    // Existing version

    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    public ?Version $targetVersion = null;

    // New version

    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    public ?VersionTypeEnum $versionType = null;

    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    public ?string $versionCode = null;

    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    public ?bool $releaseVersion = null;
}