<?php
declare(strict_types=1);

namespace App\DTO\Version;

use App\ApiResource\UpdateTypeEnum;
use App\ApiResource\VersionTypeEnum;
use App\Entity\Version;
use Symfony\Component\Serializer\Attribute\Groups;

class ExistingVersionPreparedVersionDTO extends PreparedVersionDTO
{
    public function __construct(
        Version $targetVersion,
    )
    {
        $this->updateType = UpdateTypeEnum::EXISTING_VERSION;
        $this->targetVersion = $targetVersion;
    }
}