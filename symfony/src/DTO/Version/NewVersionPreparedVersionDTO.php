<?php
declare(strict_types=1);

namespace App\DTO\Version;

use App\ApiResource\UpdateTypeEnum;
use App\ApiResource\VersionTypeEnum;
use App\Entity\Version;
use Symfony\Component\Serializer\Attribute\Groups;

class NewVersionPreparedVersionDTO extends PreparedVersionDTO
{
    public function __construct(
        VersionTypeEnum $versionType,
        string $versionCode,
        bool $releaseVersion,
    )
    {
        $this->updateType = UpdateTypeEnum::NEW_VERSION;
        $this->versionType = $versionType;
        $this->versionCode = $versionCode;
        $this->releaseVersion = $releaseVersion;
    }
}