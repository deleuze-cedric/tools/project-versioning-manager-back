<?php
declare(strict_types=1);

namespace App\DTO\Version;

use App\ApiResource\UpdateTypeEnum;
use App\ApiResource\VersionTypeEnum;
use App\Entity\Version;
use Symfony\Component\Serializer\Attribute\Groups;

class BranchPreparedVersionDTO extends PreparedVersionDTO
{
    public function __construct(
        string $branch,
        bool $pullLatestChanges,
    )
    {
        $this->updateType = UpdateTypeEnum::BRANCH;
        $this->branch = $branch;
        $this->pullLatestChanges = $pullLatestChanges;
    }
}