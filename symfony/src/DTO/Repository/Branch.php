<?php

namespace App\DTO\Repository;

use Symfony\Component\Serializer\Attribute\Groups;

class Branch
{
    public const string API_READ_GROUP = self::class.':api:read';

    #[Groups(
        self::API_READ_GROUP
    )]
    public string $title;
}