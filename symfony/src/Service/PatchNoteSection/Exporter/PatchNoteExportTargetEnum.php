<?php

namespace App\Service\PatchNoteSection\Exporter;

enum PatchNoteExportTargetEnum
{
    case CLIENT;

    case DEV;
}
