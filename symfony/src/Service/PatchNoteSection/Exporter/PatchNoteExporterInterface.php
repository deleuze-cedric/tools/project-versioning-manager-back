<?php

namespace App\Service\PatchNoteSection\Exporter;

use App\Entity\Version;

interface PatchNoteExporterInterface
{
    public function exportVersion(Version $version, PatchNoteExportTargetEnum $target): mixed;
}