<?php

namespace App\Service\PatchNoteSection\Exporter;

use App\Entity\Version;

class MarkdownPatchNoteExporter implements PatchNoteExporterInterface
{
    public function exportVersion(Version $version, PatchNoteExportTargetEnum $target): string
    {
        $str =
            "- Version: {$version->getCode()}\n".
            "- Date: {$version->getVersionedAt()->format('c')}\n\n";

        foreach ($version->getAllPatchNoteSections() as $patchNoteSection) {
            $str .= "\n## {$patchNoteSection->getName()}\n";

            foreach ($patchNoteSection->getContent() as $content){
                $str .= "- {$content}\n";
            }
        }

        return $str;
    }
}