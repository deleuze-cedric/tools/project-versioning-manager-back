<?php
declare(strict_types=1);

namespace App\Service\PatchNoteSection;

use App\Entity\PatchNoteSection;
use function Symfony\Component\String\u;

class PatchNoteSectionBuilder
{
    public function createOtherSection(): PatchNoteSection
    {
        $currentSection = new PatchNoteSection();
        return $currentSection
            ->setCode('other')
            ->setName('Autre')
            ->setRequired(true)
        ;
    }

    public function createSectionFromName(string $name): ?PatchNoteSection
    {
        $name = $this->clearSectionName($name);

        if(empty($name)){
            return  null;
        }

        $standardSection = $this->getRequiredSection($name);

        $currentSection = new PatchNoteSection();

        if ($standardSection) {
            return $currentSection
                ->setCode($standardSection['code'])
                ->setName($standardSection['name'])
                ->setRequired(true)
            ;
        }

        $code = u($name)->snake()->toString();
        return $currentSection
            ->setCode(substr($code, 0, 15))
            ->setName(substr($name, 0, 30))
            ->setRequired(false)
        ;
    }

    public function clearSectionName(string $name): string
    {
        $name = str_replace(':', '', $name);
        return trim($name);
    }

    /**
     * @return ?array{
     *     code: string,
     *     name: string
     * }
     */
    public function getRequiredSection(string $name): ?array
    {
        $name = strtolower($name);

        return match ($name) {
            'feat', 'feats',
            'feature', 'features',
            'fonctionnalité', 'fonctionnalités',
            'fonctionnalite', 'fonctionnalites' => ['code' => 'feature', 'name' => 'Fonctionnalités'],
            'fix', 'fixes',
            'patch', 'patches',
            'correctif', 'correctifs' => ['code' => 'fix', 'name' => 'Correctifs'],
            'refacto', 'refactos',
            'refactorisation', 'refactorisations' => ['code' => 'refacto', 'name' => 'Refacto'],
            'todo', 'a faire' => ['code' => 'todo', 'name' => 'TODO'],
            'internal', 'interne' => ['code' => 'internal', 'name' => 'Interne'],
            'other', 'Autre' => ['code' => 'other', 'name' => 'Autre'],
            default => null,
        };
    }
}