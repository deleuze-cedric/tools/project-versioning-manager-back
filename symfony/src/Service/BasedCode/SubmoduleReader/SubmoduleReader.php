<?php

namespace App\Service\BasedCode\SubmoduleReader;

use App\Entity\Repository;
use App\Entity\Submodule;
use App\Service\BasedCode\Handler\BasedCodeRepositoryManagerInterface;
use App\Tools\PathMerger;
use Exception;

final readonly class SubmoduleReader
{
    public function __construct(
        private PathMerger $merger,
    )
    {
    }

    public function getSubmodules(
        BasedCodeRepositoryManagerInterface $repositoryProvider,
        Repository $repository,
    ): SubmoduleListDTO
    {
        $repositoryURL = $repository->getLink();
        $submoduleFile = $repositoryProvider->findFile($repository, '.gitmodules');

        $submoduleDTO = new SubmoduleListDTO($repository, $repository->getSubModules()->toArray());

        if (!$submoduleFile) {
            return $submoduleDTO;
        }

        $repositoryCode = $repository->getCode();

        preg_match_all('/\tpath = (.*)/', $submoduleFile->content, $pathMatches);
        preg_match_all('/\turl = (.*)/', $submoduleFile->content, $urlMatches);

        foreach ($urlMatches[1] as $index => $submoduleURL) {
            $finalSubmoduleURL = $submoduleURL;

            if (!str_starts_with($submoduleURL, 'http')) {
                $repositoryURLData = parse_url($repositoryURL);
                $finalSubmoduleURL = $repositoryURLData['scheme'].'://'.$repositoryURLData['host'].$this->merger->merge($repositoryURLData['path'], $submoduleURL);
            }

            if ($repository->getSubmoduleWithLink($finalSubmoduleURL)) {
                continue;
            }

            try {
                $foundRepository = $repositoryProvider->findOneByUrl(
                    $repository->getToken(),
                    $finalSubmoduleURL
                );

                if ($foundRepository) {
                    $code = $foundRepository->getCode();

                    if ($repository->getSubmoduleWithCode($code)) {
                        continue;
                    }

                    $path = $pathMatches[1][$index];

                    $submodule = new Submodule();
                    $submodule
                        ->setCode($repositoryCode.'-'.$code)
                        ->setLink($submoduleURL)
                        ->setPath($path);

                    $repository->addSubmodule($submodule);
                    $foundRepository->addParentRepository($submodule);

                    $submoduleDTO->importableSubmodules[] = $foundRepository;
                    continue;
                }

                $unreadableRepository = new UnreadableRepositoryDTO(
                    $submoduleURL,
                    $finalSubmoduleURL,
                    '404'
                );
            } catch (Exception $e) {
                $unreadableRepository = new UnreadableRepositoryDTO(
                    $submoduleURL,
                    $finalSubmoduleURL,
                    $e->getCode()
                );
            }

            $submoduleDTO->unreadableRepositories[] = $unreadableRepository;
        }

        return $submoduleDTO;
    }
}
