<?php
declare(strict_types=1);

namespace App\Service\BasedCode\SubmoduleReader;

use App\ApiResource\BasedCode;
use Symfony\Component\Serializer\Attribute\Groups;

class UnreadableRepositoryDTO
{
    public function __construct(
        #[Groups(BasedCode::API_READ_GROUP)]
        public string $basePath,
        #[Groups(BasedCode::API_READ_GROUP)]
        public string $finalUrl,
        #[Groups(BasedCode::API_READ_GROUP)]
        public string $reason,
    )
    {
    }
}