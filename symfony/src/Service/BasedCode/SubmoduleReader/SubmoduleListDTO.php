<?php
declare(strict_types=1);

namespace App\Service\BasedCode\SubmoduleReader;

use ApiPlatform\Metadata\ApiProperty;
use App\ApiResource\BasedCode;
use App\Entity\Repository;
use SplObjectStorage;
use Symfony\Component\Serializer\Attribute\Groups;

class SubmoduleListDTO
{
    /**
     * @param iterable<Repository> $importableSubmodules
     * @param iterable<UnreadableRepositoryDTO> $unreadableRepositories
     */
    public function __construct(
        #[Groups(BasedCode::API_READ_GROUP)]
        #[ApiProperty(identifier: true)]
        public Repository $parentRepository,
        #[Groups(BasedCode::API_READ_GROUP)]
        public array $importableSubmodules = [],
        #[Groups(BasedCode::API_READ_GROUP)]
        public array $unreadableRepositories = []
    ) {
    }
}
