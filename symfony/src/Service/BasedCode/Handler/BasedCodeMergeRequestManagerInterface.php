<?php
declare(strict_types=1);

namespace App\Service\BasedCode\Handler;

use App\Entity\MergeRequest;
use App\Entity\Repository;
use App\Service\BasedCode\DTO\BasedCodeFileDTO;
use App\Service\BasedCode\SubmoduleReader\SubmoduleListDTO;

interface BasedCodeMergeRequestManagerInterface
{
    public function findOne(?string $host, ?string $token, string $code): ?MergeRequest;

    public function findOrCreateMergeRequest(Repository $repository, string $source, string $target, string $title, bool $squash): MergeRequest;

    public function mergeMergeRequest(MergeRequest $mergeRequest): MergeRequest;
}
