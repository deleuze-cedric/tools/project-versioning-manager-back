<?php
declare(strict_types=1);

namespace App\Service\BasedCode\Handler;

use App\DTO\Repository\Branch;
use App\Entity\MergeRequest;
use App\Entity\Repository;
use App\Service\BasedCode\DTO\BasedCodeFileDTO;
use App\Service\BasedCode\SubmoduleReader\SubmoduleListDTO;

interface BasedCodeRepositoryManagerInterface
{
    /**
     * @return array<Repository>
     */
    public function find(?string $host, ?string $token, string $search = '', int $page = 1): array;

    public function findOne(?string $host, ?string $token, string $code): ?Repository;

    public function findOneByUrl(?string $token, string $url): ?Repository;

    public function findSubModules(Repository $repository): SubmoduleListDTO;

    public function findFile(Repository $repository, string $file): ?BasedCodeFileDTO;

    /** @return iterable<MergeRequest> */
    public function findMergeRequests(Repository $repository, bool $isMerged, ?string $search = null): iterable;

    public function createBranchIfNotExists(Repository $repository, string $sourceBranch, string $targetBranch): void;

    /**
     * @return string must be the commit_sha for tag
     */
    public function createTag(Repository $repository, string $branch, string $code, string $content): string;

    public function createRelease(Repository $repository, string $tagName, string $code, string $content): void;

    /** @return iterable<Branch> */
    public function searchBranch(Repository $repository, string $search): iterable;
}
