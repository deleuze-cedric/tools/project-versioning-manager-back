<?php
declare(strict_types=1);

namespace App\Service\BasedCode\Handler;

use App\Entity\MergeRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

interface MergeRequestSynchronizerFromWebhookInterface
{
    /**
     * @throws NotFoundHttpException
     */
    public function sync(Request $request): MergeRequest;
}
