<?php

namespace App\Service\BasedCode\Handler;

use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag(self::TAG)]
interface BasedCodeHandlerInterface
{
    public const TAG = 'app.based_code.handler';

    public function getIdentifier(): string;

    public function getName(): string;

    public function getRepositoryManager(): BasedCodeRepositoryManagerInterface;

    public function getMergeRequestManager(): BasedCodeMergeRequestManagerInterface;

    public function getMergeRequestSynchronizerFromWebhook(): MergeRequestSynchronizerFromWebhookInterface;
}