<?php

namespace App\Service\BasedCode\Handler\Gitlab\Synchronizer;

use App\Entity\MergeRequest;
use App\Repository\MergeRequestRepository;
use App\Repository\RepositoryRepository;
use App\Service\BasedCode\Handler\Gitlab\Builder\GitlabMergeRequestMapper;
use App\Service\BasedCode\Handler\MergeRequestSynchronizerFromWebhookInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

final readonly class GitlabMergeRequestSynchronizerFromWebhook implements MergeRequestSynchronizerFromWebhookInterface
{
    public function __construct(
        private RepositoryRepository $repositoryRepository,
        private MergeRequestRepository $mergeRequestRepository,
        private EntityManagerInterface $entityManager,
        private GitlabMergeRequestMapper $gitlabMergeRequestMapper,
    )
    {
    }

    public function sync(Request $request): MergeRequest
    {
        $body = $request->getContent();
        $data = json_decode($body);

        $repositoryCode = (string)$data->project?->id;
        $repository = $this->repositoryRepository->findOneByCode($repositoryCode);

        if (!$repository) {
            throw new NotFoundHttpException();
        }

        // TODO : Secure with X-Gitlab-Token

        $attributes = $data->object_attributes;
        $mrCode = (string)$attributes?->iid;

        $mergeRequest = $this->mergeRequestRepository->findOneByCode($mrCode) ?? new MergeRequest();
        $version = $mergeRequest->getVersion() ?? $repository->getDevVersion();
        $this->gitlabMergeRequestMapper->mapFromAPI($attributes, $version, $mergeRequest);

        if (!$mergeRequest->targetBranchIsBranchUsedToVersion()) {
            $this->entityManager->remove($mergeRequest);
            throw new UnprocessableEntityHttpException();
        }

        switch ($attributes?->action) {
            case 'close':
                $mergeRequest->setClosedAt(new \DateTimeImmutable());
                break;
            case 'reopen':
                $mergeRequest->setClosedAt(null);
                break;
            case 'merge':
                $mergeRequest->setMergedAt(new \DateTimeImmutable());
                break;
        }

        $this->entityManager->persist($mergeRequest);
        $this->entityManager->flush();

        return $mergeRequest;
    }
}