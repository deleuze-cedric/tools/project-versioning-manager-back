<?php
declare(strict_types=1);

namespace App\Service\BasedCode\Handler\Gitlab;

use App\DTO\Repository\Branch;
use App\Entity\Repository;
use App\Service\BasedCode\DTO\BasedCodeFileDTO;
use App\Service\BasedCode\Handler\BasedCodeRepositoryManagerInterface;
use App\Service\BasedCode\Handler\Gitlab\Api\GitlabProjectApiRepository;
use App\Service\BasedCode\SubmoduleReader\SubmoduleListDTO;
use App\Service\BasedCode\SubmoduleReader\SubmoduleReader;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

final readonly class GitlabRepositoryManager implements BasedCodeRepositoryManagerInterface
{
    public function __construct(
        private GitlabProjectApiRepository $gitlabProjectApiRepository,
        private SubmoduleReader $submoduleReader,
    )
    {
    }

    /**
     * @return array<Repository>
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function find(?string $host, ?string $token, string $search = '', int $page = 1): array
    {
        return $this->gitlabProjectApiRepository
            ->initGitlabApi($host, $token)
            ->findProjects($search, $page);
    }

    public function findOne(?string $host, ?string $token, string $code): ?Repository
    {
        return $this->gitlabProjectApiRepository
            ->initGitlabApi($host, $token)
            ->findProject($code);
    }

    public function findSubModules(Repository $repository): SubmoduleListDTO
    {
        return $this->submoduleReader->getSubmodules($this, $repository);
    }

    public function findFile(Repository $repository, string $file): ?BasedCodeFileDTO
    {
        return $this->gitlabProjectApiRepository
            ->initGitlabApiFromRepository($repository)
            ->findFile(
                $repository->getCode(),
                $repository->getMainBranch(),
                $file
            );
    }

    public function findOneByUrl(?string $token, string $url): ?Repository
    {
        $urlData = parse_url($url);
        $host = $urlData['host'];
        $path = $urlData['path'];

        if (str_starts_with($path, '/')) {
            $path = substr($path, 1);
        }

        $path = urlencode($path);

        if (str_ends_with($path, '.git')) {
            $path = substr($path, 0, -4);
        }

        return $this->findOne($host, $token, $path);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function findMergeRequests(Repository $repository, bool $isMerged, ?string $search = null): iterable
    {
        return $this->gitlabProjectApiRepository
            ->initGitlabApiFromRepository($repository)
            ->findMergeRequests($repository, $isMerged, $search);
    }

    public function createBranchIfNotExists(Repository $repository, string $sourceBranch, string $targetBranch): void
    {
        $branchExists = $this->gitlabProjectApiRepository
            ->initGitlabApiFromRepository($repository)
            ->searchBranch($repository, $targetBranch);

        if (!$branchExists) {
            $this->gitlabProjectApiRepository
                ->createBranch($repository, $sourceBranch, $targetBranch);
        }
    }

    public function createTag(Repository $repository, string $branch, string $code, string $content): string
    {
        return $this->gitlabProjectApiRepository
            ->initGitlabApiFromRepository($repository)
            ->createTag($repository, $branch, $code, $content)->commit->id;
    }

    public function createRelease(Repository $repository, string $tagName, string $code, string $content): void
    {
        $this->gitlabProjectApiRepository
            ->initGitlabApiFromRepository($repository)
            ->createRelease($repository, $tagName, $code, $content);
    }

    /**
     * @return iterable<Branch>
     */
    public function searchBranch(Repository $repository, string $search): iterable
    {
        return $this->gitlabProjectApiRepository
            ->initGitlabApiFromRepository($repository)
            ->searchBranch($repository, $search);
    }

    public function updateSubmoduleVersion(Repository $repository, Repository $submodule, string $branch, string $commit, string $message): void
    {
        $this->gitlabProjectApiRepository
            ->initGitlabApiFromRepository($repository)
            ->updateSubmoduleVersion($repository, $submodule, $branch, $commit, $message);
    }
}
