<?php
declare(strict_types=1);

namespace App\Service\BasedCode\Handler\Gitlab\Api;

use App\Entity\Repository;
use Monolog\Attribute\WithMonologChannel;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpOptions;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\Service\Attribute\Required;

#[WithMonologChannel('based_code.gitlab.client')]
abstract class GitlabClient
{

    private readonly HttpClientInterface $client;

    protected readonly LoggerInterface $logger;

    public string $host;

    public ?string $token = null;

    #[Required]
    public function init(HttpClientInterface $client, LoggerInterface $logger): void
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @throws TransportExceptionInterface
     */
    protected function get(string $route): ResponseInterface
    {
        $this->logger->debug('Rqt GET to gitlab', ['route' => $route, 'token' => $this->token]);
        return $this->getClient()->request('GET', $route);
    }

    /**
     * @throws TransportExceptionInterface
     */
    protected function post(string $route, array $data = []): ResponseInterface
    {
        $this->logger->debug('Rqt POST to gitlab', ['route' => $route, 'token' => $this->token]);
        return $this->getClient()->request(
            'POST',
            $route,
            [
                'body' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );
    }

    /**
     * @throws TransportExceptionInterface
     */
    protected function put(string $route, array $data): ResponseInterface
    {
        $this->logger->debug('Rqt POST to gitlab', ['route' => $route, 'token' => $this->token]);
        return $this->getClient()->request(
            'PUT',
            $route,
            [
                'body' => $data,
                'headers' => ['Content-Type' => 'application/json'],
            ]
        );
    }

    protected function getClient(): HttpClientInterface
    {
        $options = new HttpOptions();
        $options->setBaseUri($this->getBaseUri())
            ->setHeaders($this->buildHeaders());

        return $this->client->withOptions($options->toArray());
    }

    protected function buildHeaders(): array
    {
        $headers = [];

        if ($this->token) {
            $headers['Authorization'] = 'Bearer '.$this->token;
        }

        return $headers;
    }

    protected function getBaseUri(): string
    {
        return 'https://'.$this->host.'/api/v4/';
    }

    public function initGitlabApi(?string $host, ?string $token): self
    {
        $this->token = $token;
        $this->host = $host ?? 'gitlab.com';
        return $this;
    }

    public function initGitlabApiFromRepository(Repository $repository): self
    {
        return $this->initGitlabApi(
            $repository->getHost(),
            $repository->getToken()
        );
    }
}
