<?php
declare(strict_types=1);

namespace App\Service\BasedCode\Handler\Gitlab\Api;

use App\Entity\MergeRequest;
use App\Entity\Repository;
use App\Service\BasedCode\Handler\Gitlab\Builder\GitlabMergeRequestMapper;
use Monolog\Attribute\WithMonologChannel;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

#[WithMonologChannel('based_code.gitlab.merge_request_api_repository')]
final class GitlabMergeRequestApiRepository extends GitlabClient
{
    public function __construct(
        private readonly GitlabMergeRequestMapper $mergeRequestGitlabMapper,
    )
    {
    }

    public function findMergeRequest(string $code): ?MergeRequest
    {
        $url = 'merge_requests/'.$code;

        try {
            $response = $this->get($url);
            $mergeRequestData = json_decode($response->getContent());
            return $this->mergeRequestGitlabMapper->mapFromAPI($mergeRequestData, null);
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface) {
            $this->logger->info('Cannot find project with code : '.$code);
            return null;
        }
    }

    public function searchMergeRequestBySourceAndTargetBranch(Repository $repository, string $sourceBranch, string $targetBranch, string $state): ?MergeRequest
    {
        $url = "projects/{$repository->getCode()}/merge_requests/?source_branch=$sourceBranch&target_branch=$targetBranch&state=$state";

        try {
            $response = $this->get($url);
            $mergeRequestData = json_decode($response->getContent());

            if (count($mergeRequestData) < 1) {
                return null;
            }

            return $this->mergeRequestGitlabMapper->mapFromAPI($mergeRequestData[0], $repository->getLatestVersion() ?? $repository->getDevVersion());
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface) {
            $this->logger->info('Cannot find merge request', ['source_branch' => $sourceBranch, 'target_branch' => $targetBranch, 'state' => $state]);
            return null;
        }
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws \Exception
     */
    public function createMergeRequest(Repository $repository, string $sourceBranch, string $targetBranch, string $title, bool $squash): ?MergeRequest
    {
        $url = "projects/{$repository->getCode()}/merge_requests?source_branch=$sourceBranch&target_branch=$targetBranch&title=$title&squash=$squash";

        $response = $this->post($url);
        $mergeRequestData = json_decode($response->getContent());

        return $this->mergeRequestGitlabMapper->mapFromAPI($mergeRequestData, $repository->getLatestVersion() ?? $repository->getDevVersion());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws \Exception
     */
    public function mergeMergeRequest(MergeRequest $mergeRequest): ?MergeRequest
    {
        $url = "projects/{$mergeRequest->getRepository()->getCode()}/merge_requests/{$mergeRequest->getCode()}/merge";
        $response = $this->put($url);
        $mergeRequestData = json_decode($response->getContent());

        return $this->mergeRequestGitlabMapper->mapFromAPI($mergeRequestData, null);
    }
}
