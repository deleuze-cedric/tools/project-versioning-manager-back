<?php
declare(strict_types=1);

namespace App\Service\BasedCode\Handler\Gitlab\Api;

use App\DTO\Repository\Branch;
use App\Entity\MergeRequest;
use App\Entity\Repository;
use App\Repository\RepositoryRepository;
use App\Service\BasedCode\DTO\BasedCodeFileDTO;
use App\Service\BasedCode\Handler\Gitlab\Builder\GitlabMergeRequestMapper;
use App\Service\BasedCode\Handler\Gitlab\GitlabHandler;
use Exception;
use Monolog\Attribute\WithMonologChannel;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;

#[WithMonologChannel('based_code.gitlab.project_api_repository')]
final class GitlabProjectApiRepository extends GitlabClient
{
    public function __construct(
        private readonly GitlabMergeRequestMapper $mergeRequestGitlabMapper,
        private readonly RepositoryRepository $repositoryRepository,
    )
    {
    }

    /**
     * @return array<Repository>
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function findProjects(string $search = null, int $page = 1, int $pagination = 10, bool $membership = true): array
    {
        $url = 'projects?simple=true';

        if ($membership) {
            $url .= '&membership=true';
        }

        if ($search) {
            $url .= '&search='.$search;
        }

        $url .= '&pagination=true&per_page='.$pagination.'&page='.$page;

        $response = $this->get($url);
        $projects = json_decode($response->getContent());
        $repositories = [];

        foreach ($projects as $project) {
            $id = (string)$project->id;
            $basedCodeIdentifier = GitlabHandler::IDENTIFIER;
            $repository = $this->repositoryRepository->findOneBy(['code' => $id, 'basedCodeIdentifier' => $basedCodeIdentifier]) ?? new Repository();
            $repositories[] = $repository->setName($project->name)
                ->setCode($id)
                ->setLink($project->web_url)
                ->setToken($this->token)
                ->setMainBranch($project->default_branch)
                ->setHost($this->host)
                ->setBasedCodeIdentifier($basedCodeIdentifier);
        }

        return $repositories;
    }

    public function findProject(string $code): ?Repository
    {
        $url = 'projects/'.$code;

        try {
            $response = $this->get($url);
            $project = json_decode($response->getContent());

            $id = (string)$project->id;
            $basedCodeIdentifier = GitlabHandler::IDENTIFIER;
            $repository = $this->repositoryRepository->findOneBy(['code' => $id, 'basedCodeIdentifier' => $basedCodeIdentifier]) ?? new Repository();

            return $repository->setName($project->name)
                ->setCode($id)
                ->setLink($project->web_url)
                ->setToken($this->token)
                ->setMainBranch($project->default_branch)
                ->setHost($this->host)
                ->setBasedCodeIdentifier($basedCodeIdentifier);
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface) {
            $this->logger->info('Cannot find project with code : '.$code);
            return null;
        }
    }

    public function findFile(string $projectCode, string $branch, string $filename): ?BasedCodeFileDTO
    {
        $url = 'projects/'.$projectCode.'/repository/files/'.$filename.'?ref='.$branch;

        try {
            $response = $this->get($url);
            $decodedContent = json_decode($response->getContent());

            return new BasedCodeFileDTO($filename, base64_decode($decodedContent->content));
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface) {
            $this->logger->info('Cannot find file "'.$filename.'" for project "'.$projectCode.'" on branch "'.$branch.'"');
            return null;
        }
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function findMergeRequests(Repository $repository, bool $isMerged = null, ?string $search = null): array
    {
        $state = $isMerged ? 'merged' : 'opened';
        $url = 'projects/'.$repository->getCode().'/merge_requests?state='.$state;

        if ($search) {
            $url .= '&search='.$search.'&in=title';
        }

        $response = $this->get($url);
        $decodedContent = json_decode($response->getContent());

        $latestVersion = $repository->getDevVersion();
        $mergeRequests = [];

        foreach ($decodedContent as $mergeRequestData) {
            $mergeRequests[] = $this->mergeRequestGitlabMapper->mapFromAPI($mergeRequestData, $latestVersion);
        }

        return $mergeRequests;
    }

    public function createBranch(Repository $repository, string $sourceBranch, string $targetBranch): void
    {
        $url = "projects/{$repository->getCode()}/repository/branches?branch=$targetBranch&ref=$sourceBranch";
        $this->post($url);
    }

    /**
     * @return object{
     *     commit: object{
     *       id: string,
     *     }
     * }
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function createTag(Repository $repository, string $branch, string $name, string $message): object
    {
        $url = "projects/{$repository->getCode()}/repository/tags?tag_name=$name";

        $content = $this->post($url, [
            "ref" => $branch,
            "message" => $message,
        ])->getContent();

        return json_decode($content);
    }

    public function createRelease(Repository $repository, string $tagName, string $name, string $message): void
    {
        $url = "projects/{$repository->getCode()}/releases";
        $this->post($url, [
            "name" => $name,
            "tag_name" => $tagName,
            "description" => $message,
        ]);
    }

    /**
     * @return iterable<Branch>
     */
    public function searchBranch(Repository $repository, string $search): iterable
    {
        $url = "projects/{$repository->getCode()}/repository/branches?search=$search";
        $this->get($url);

        $response = $this->get($url);
        $decodedContent = json_decode($response->getContent());
        $branches = [];

        foreach ($decodedContent as $branchFromApi) {
            $branch = new Branch();
            $branch->title = $branchFromApi->name;
            $branches[] = $branch;
        }

        return $branches;
    }

    public function updateSubmoduleVersion(Repository $repository, Repository $submodule, string $branch, string $commit, string $message): void
    {
        $url = "projects/{$repository->getCode()}/repository/submodules/{$submodule->getPath()}";

        $data = [
            'branch' => $branch,
            'commit_sha' => $commit,
            'commit_message' => $message,
        ];

        try {
            $this->put($url, $data);
        } catch (TransportException|ClientException $e) {
            $message = $e->getResponse()->toArray(false)['message'];
            $data['submodule'] = $submodule->getPath();
            $separator = "\n- ";
            $data_str = implode($separator, $data);
            throw new Exception($message.$separator.$data_str);
        }
    }
}
