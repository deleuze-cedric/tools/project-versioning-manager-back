<?php
declare(strict_types=1);

namespace App\Service\BasedCode\Handler\Gitlab\Builder;

use App\Entity\MergeRequest;
use App\Entity\Version;
use DateTimeImmutable;
use Exception;

final readonly class GitlabMergeRequestMapper
{
    /**
     * @throws Exception
     */
    public function mapFromAPI(
        object $data,
        ?Version $version,
        MergeRequest $mergeRequest = new MergeRequest(),
    ): MergeRequest
    {
        $mergeAtValue = $data->merged_at ?? null;
        $mergeAtDateTime = $mergeAtValue ? new DateTimeImmutable($mergeAtValue) : null;
        // $description = $data->description;
        // Usefull if we want to import patch note from description
        // $patchNoteSections = $this->mergeRequestDescriptionReader->read($description);

        return $mergeRequest
            ->setVersion($version)
            ->setName(substr($data->title, 0, 150))
            ->setLink($data->web_url ?? $data->url)
            ->setCode((string)$data->iid)
            ->setMergedAt($mergeAtDateTime)
            // ->setPatchNoteSections($patchNoteSections)
            ->setSourceBranch($data->source_branch)
            ->setTargetBranch($data->target_branch);

        // TODO : add mr dependencies => impossible pour le moment
        // https://gitlab.com/groups/gitlab-org/-/epics/8173
    }
}