<?php

namespace App\Service\BasedCode\Handler\Gitlab;

use App\Service\BasedCode\Handler\BasedCodeHandlerInterface;
use App\Service\BasedCode\Handler\BasedCodeMergeRequestManagerInterface;
use App\Service\BasedCode\Handler\MergeRequestSynchronizerFromWebhookInterface;
use App\Service\BasedCode\Handler\BasedCodeRepositoryManagerInterface;
use App\Service\BasedCode\Handler\Gitlab\Synchronizer\GitlabMergeRequestSynchronizerFromWebhook;

final readonly class GitlabHandler implements BasedCodeHandlerInterface
{
    public const IDENTIFIER = 'gitlab';
    public const NAME = 'Gitlab';

    public function __construct(
        private GitlabRepositoryManager $repositoryProvider,
        private GitlabMergeRequestManager $mergeRequestProvider,
        private GitlabMergeRequestSynchronizerFromWebhook $mergeRequestSynchronizer
    ) {
    }

    public function getIdentifier(): string
    {
        return self::IDENTIFIER;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getRepositoryManager(): BasedCodeRepositoryManagerInterface
    {
        return $this->repositoryProvider;
    }

    public function getMergeRequestManager(): BasedCodeMergeRequestManagerInterface
    {
        return $this->mergeRequestProvider;
    }

    public function getMergeRequestSynchronizerFromWebhook(): MergeRequestSynchronizerFromWebhookInterface
    {
        return $this->mergeRequestSynchronizer;
    }
}
