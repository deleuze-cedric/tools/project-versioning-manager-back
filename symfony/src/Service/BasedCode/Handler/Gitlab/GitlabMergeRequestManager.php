<?php
declare(strict_types=1);

namespace App\Service\BasedCode\Handler\Gitlab;

use App\Entity\MergeRequest;
use App\Entity\Repository;
use App\Service\BasedCode\Handler\BasedCodeMergeRequestManagerInterface;
use App\Service\BasedCode\Handler\Gitlab\Api\GitlabMergeRequestApiRepository;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

final readonly class GitlabMergeRequestManager implements BasedCodeMergeRequestManagerInterface
{
    public function __construct(
        private GitlabMergeRequestApiRepository $gitlabMergeRequestApiRepository,
    )
    {
    }

    public function findOne(?string $host, ?string $token, string $code): ?MergeRequest
    {
        return $this->gitlabMergeRequestApiRepository
            ->initGitlabApi($host, $token)
            ->findMergeRequest($code);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function findOrCreateMergeRequest(Repository $repository, string $source, string $target, string $title, bool $squash): MergeRequest
    {
        $mergeRequest = $this->gitlabMergeRequestApiRepository
            ->initGitlabApiFromRepository($repository)
            ->searchMergeRequestBySourceAndTargetBranch($repository, $source, $target, 'opened');

        if ($mergeRequest) {
            return $mergeRequest;
        }

        return $this->gitlabMergeRequestApiRepository->createMergeRequest($repository, $source, $target, $title, $squash);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function mergeMergeRequest(MergeRequest $mergeRequest): MergeRequest
    {
        return $this->gitlabMergeRequestApiRepository
            ->initGitlabApiFromRepository($mergeRequest->getRepository())
            ->mergeMergeRequest($mergeRequest);
    }
}
