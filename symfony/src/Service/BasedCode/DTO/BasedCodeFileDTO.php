<?php

namespace App\Service\BasedCode\DTO;

class BasedCodeFileDTO
{
    public function __construct(
        public string $name,
        public string $content
    ) {
    }
}
