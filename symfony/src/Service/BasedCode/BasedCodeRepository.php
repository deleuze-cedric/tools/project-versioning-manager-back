<?php

namespace App\Service\BasedCode;

use App\ApiResource\BasedCode;
use App\Entity\Repository;
use App\Service\BasedCode\Handler\BasedCodeHandlerInterface;
use Monolog\Attribute\WithMonologChannel;
use Symfony\Component\DependencyInjection\Attribute\AutowireIterator;

#[WithMonologChannel('based_code.repository')]
final readonly class BasedCodeRepository
{
    /** @var iterable<BasedCodeHandlerInterface> $basedCodeHandlers */
    public function __construct(
        #[AutowireIterator(BasedCodeHandlerInterface::TAG)] private iterable $basedCodeHandlers
    ) {
    }

    /**
     * @return iterable<BasedCode>
     */
    public function find(): iterable
    {
        $basedCodeApiResources = [];

        foreach ($this->basedCodeHandlers as $basedCodeHandler){
            $basedCodeEntity = new BasedCode();
            $basedCodeEntity->setBasedCodeHandler($basedCodeHandler);
            $basedCodeApiResources[] = $basedCodeEntity;
        }

        return $basedCodeApiResources;
    }

    public function findById(string $identifier): ?BasedCode
    {
        foreach ($this->find() as $basedCode){
            if($identifier === $basedCode->getId()){
                return $basedCode;
            }
        }

        return null;
    }

    public function findForRepository(Repository $repository): ?BasedCode
    {
        $identifier = $repository->getBasedCodeIdentifier();
        return $this->findById($identifier);
    }
}