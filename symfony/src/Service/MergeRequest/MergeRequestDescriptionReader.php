<?php
declare(strict_types=1);

namespace App\Service\MergeRequest;

use App\Entity\MergeRequest;
use App\Entity\PatchNoteSection;
use App\Service\PatchNoteSection\PatchNoteSectionBuilder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use SplFileObject;
use SplTempFileObject;

final readonly class MergeRequestDescriptionReader
{
    public function __construct(
        private PatchNoteSectionBuilder $patchNoteSectionBuilder
    )
    {
    }

    /**
     * @return Collection<PatchNoteSection>
     */
    public function read(string $description): Collection
    {
        $file = new SplTempFileObject();
        $file->fwrite($description);
        $file->fseek(0);
        $file->setFlags(SplFileObject::SKIP_EMPTY);
        $sections = new ArrayCollection();
        $currentSection = $this->patchNoteSectionBuilder->createOtherSection();
        $contentLine = '';

        while (!$file->eof()) {
            $line = trim($file->fgets());

            if (empty($line)) {
                continue;
            }

            if (preg_match('/^##([^#].*)/', $line, $sectionName)) {
                $newSection = $this->patchNoteSectionBuilder->createSectionFromName($sectionName[1]);

                if (!$newSection) {
                    continue;
                }

                $this->addContentLine($currentSection, $contentLine);
                $this->addSection($sections, $currentSection);

                $currentSection = $newSection;
                $contentLine = '';
            } else if (preg_match('/^[-*](.*)/', $line, $content)) {
                $content = trim($content[1]);

                $this->addContentLine($currentSection, $contentLine);

                $contentLine = $content;
            } else {
                $contentLine .= (!empty($contentLine) ? "\n" : '').trim($line);
            }
        }

        $this->addContentLine($currentSection, $contentLine);
        $this->addSection($sections, $currentSection);

        return $sections;
    }

    private function addContentLine(PatchNoteSection $section, string $contentLine): void
    {
        if (!empty($contentLine)) {
            $section->addContentLine($contentLine);
        }
    }

    private function addSection(Collection $sections, PatchNoteSection $section): void
    {
        if (!$section->isEmpty()) {
            $sections->add($section);
        }
    }
}