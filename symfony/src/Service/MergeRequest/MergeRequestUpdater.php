<?php

namespace App\Service\MergeRequest;

use App\Entity\MergeRequest;

class MergeRequestUpdater
{
    public function updateFromRemote(MergeRequest $mergeRequestInDB, MergeRequest $mergeRequestFromRemote): void
    {
        $mergeRequestInDB
            ->setName(substr($mergeRequestFromRemote->getName(), 0, 150))
            // ->setPatchNoteSections($mergeRequestFromRemote->getPatchNoteSections())
        ;
    }
}