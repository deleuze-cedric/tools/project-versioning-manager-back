<?php

namespace App\Service\Version\VersionBuilder;

use App\DTO\Version\PreparedVersionDTO;
use App\Entity\SubmoduleNewVersion;
use App\Entity\Version;
use App\Service\BasedCode\BasedCodeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Contracts\Service\Attribute\Required;

readonly class VersionReleaser
{
    private BasedCodeRepository $basedCodeRepository;

    private EntityManagerInterface $entityManager;

    /** @var iterable<UpdateTypeVersionReleaserInterface> $typeReleasers */
    private iterable $typeReleasers;

    #[Required]
    public function init(
        EntityManagerInterface $entityManager,
        /** @param iterable<UpdateTypeVersionReleaserInterface> $typeReleasers */
        #[TaggedIterator(UpdateTypeVersionReleaserInterface::TAG)] iterable $typeReleasers,
        BasedCodeRepository $basedCodeRepository,
    ): void
    {
        $this->basedCodeRepository = $basedCodeRepository;
        $this->typeReleasers = $typeReleasers;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws Exception
     */
    public function releaseVersion(PreparedVersionDTO $preparedVersion): Version
    {
        // TODO : validate version DTO (code, ...)

        $repository = $preparedVersion->repository;
        $basedCode = $this->basedCodeRepository->findForRepository($repository);
        $basedCodeHandler = $basedCode->getBasedCodeHandler();
        $mergeRequestManager = $basedCodeHandler->getMergeRequestManager();
        $repositoryManager = $basedCodeHandler->getRepositoryManager();
        // $mainBranch = $repository->getMainBranch();
        // $devBranch = $repository->getDevBranch();

        $submodulesVersions = [];

        foreach ($preparedVersion->submoduleReleases as $subModuleRelease) {
            $submoduleRepository = $subModuleRelease->repository;
            $newSubmoduleVersion = $this->releaseVersion($subModuleRelease);
            // TODO : enabled it if we want to manage code in repository
            // $versionCommit = $this->releaseVersion($subModuleRelease);
            // $repositoryManager->updateSubmoduleVersion(
            //     repository: $repository,
            //     submodule: $subModuleRelease->repository,
            //     branch: $devBranch ?: $mainBranch,
            //     commit: $version->getCommitSha(),
            //     message: 'Auto update '.$subModuleRelease->repository->getName().' module'
            // );

            $submodulesVersions[] = (new SubmoduleNewVersion())
                ->setSubmodule($submoduleRepository)
                ->setSubmoduleVersion($newSubmoduleVersion);
        }

        foreach ($this->typeReleasers as $typeReleaser) {
            if ($typeReleaser->getSupportedUpdateType() === $preparedVersion->updateType) {
                $version = $typeReleaser->release($preparedVersion, $repositoryManager, $mergeRequestManager);

                foreach ($submodulesVersions as $submodulesVersion) {
                    $version->addSubmoduleVersion($submodulesVersion);
                    $this->entityManager->persist($submodulesVersion);
                }

                $this->entityManager->persist($version);
                $this->entityManager->flush();

                return $version;
            }
        }

        throw new Exception('Unsupported update type: '.$preparedVersion->updateType->name);
    }
}