<?php

namespace App\Service\Version\VersionBuilder;

use App\ApiResource\UpdateTypeEnum;
use App\DTO\Version\PreparedVersionDTO;
use App\Entity\Version;
use App\Service\BasedCode\Handler\BasedCodeMergeRequestManagerInterface;
use App\Service\BasedCode\Handler\BasedCodeRepositoryManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag(self::TAG)]
interface UpdateTypeVersionReleaserInterface
{
    public const string TAG = 'app.update_type_version_releaser';

    public function getSupportedUpdateType(): UpdateTypeEnum;

    public function release(
        PreparedVersionDTO $preparedVersionDTO,
        BasedCodeRepositoryManagerInterface $repositoryManager,
        BasedCodeMergeRequestManagerInterface $mergeRequestManager,
    ): Version;
}