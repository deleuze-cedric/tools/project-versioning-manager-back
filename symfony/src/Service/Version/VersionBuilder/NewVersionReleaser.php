<?php
declare(strict_types=1);

namespace App\Service\Version\VersionBuilder;

use App\ApiResource\UpdateTypeEnum;
use App\DTO\Version\PreparedVersionDTO;
use App\Entity\Repository;
use App\Entity\Version;
use App\Service\BasedCode\Handler\BasedCodeMergeRequestManagerInterface;
use App\Service\BasedCode\Handler\BasedCodeRepositoryManagerInterface;
use App\Service\PatchNoteSection\Exporter\MarkdownPatchNoteExporter;
use App\Service\PatchNoteSection\Exporter\PatchNoteExportTargetEnum;
use App\Service\Repository\MergeRequestSynchronizer;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use function Symfony\Component\String\u;

readonly class NewVersionReleaser implements UpdateTypeVersionReleaserInterface
{
    public function __construct(
        private MergeRequestSynchronizer $mergeRequestSynchronizer,
        private MarkdownPatchNoteExporter $markdownPatchNoteExporter,
        private EntityManagerInterface $entityManager,
    )
    {
    }

    public function getSupportedUpdateType(): UpdateTypeEnum
    {
        return UpdateTypeEnum::NEW_VERSION;
    }

    /**
     * @throws Exception
     */
    public function release(
        PreparedVersionDTO $preparedVersionDTO,
        BasedCodeRepositoryManagerInterface $repositoryManager,
        BasedCodeMergeRequestManagerInterface $mergeRequestManager,
    ): Version
    {
        $repository = $preparedVersionDTO->repository;

        // Be sure that all merge requests have status up to date
        $this->mergeRequestSynchronizer->syncRepository($repository, true, true);

        $preparedVersionDTO->versionCode = u($preparedVersionDTO->versionCode)->replace(' ', '-')->__toString();

        $releasedVersion = $this->releaseVersionInDatabase($preparedVersionDTO);
        $this->releaseVersionInBasedCode($preparedVersionDTO, $releasedVersion, $repositoryManager, $mergeRequestManager);

        return $releasedVersion;
    }

    private function releaseVersionInDatabase(PreparedVersionDTO $preparedVersionDTO): Version
    {
        $repository = $preparedVersionDTO->repository;
        $versionReleased = $repository->getDevVersion();

        $versionReleased
            ->setCode($preparedVersionDTO->versionCode)
            ->setVersionedAt(new \DateTimeImmutable());

        $this->entityManager->persist($versionReleased);
        $this->entityManager->flush();

        $newDevVersion = $repository->getDevVersion();
        $this->transferNotMergedMergeRequestsToNewDevVersion($versionReleased, $newDevVersion);

        $this->entityManager->persist($versionReleased);
        $this->entityManager->persist($newDevVersion);
        $this->entityManager->flush();

        return $versionReleased;
    }

    private function transferNotMergedMergeRequestsToNewDevVersion(Version $versionReleased, Version $newDevVersion): void
    {
        $mergeRequests = $versionReleased->getMergeRequests();

        foreach ($mergeRequests as $mergeRequest) {
            if (!$mergeRequest->isMerged()) {
                $versionReleased->removeMergeRequest($mergeRequest);
                $newDevVersion->addMergeRequest($mergeRequest);
                $this->entityManager->persist($mergeRequest);
            }
        }
    }

    private function releaseVersionInBasedCode(
        PreparedVersionDTO $preparedVersionDTO,
        Version $versionReleased,
        BasedCodeRepositoryManagerInterface $repositoryManager,
        BasedCodeMergeRequestManagerInterface $mergeRequestManager,
    ): string
    {
        $repository = $preparedVersionDTO->repository;

        // TODO : enabled it if we want to manage code in repository
        // if ($repository->getDevBranch()) {
        //     $this->mergeDevIntoMain($repository, $mergeRequestManager, $repositoryManager);
        // }

        return $this->createRelease($repository, $preparedVersionDTO, $versionReleased, $repositoryManager);
    }

    private function mergeDevIntoMain(
        Repository $repository,
        BasedCodeMergeRequestManagerInterface $mergeRequestManager,
        BasedCodeRepositoryManagerInterface $repositoryManager,
    ): void
    {
        $devBranch = $repository->getDevBranch();
        $mainBranch = $repository->getMainBranch();

        $repositoryManager->createBranchIfNotExists($repository, $mainBranch, $devBranch);
        $mergeRequestFromDevToMain = $mergeRequestManager->findOrCreateMergeRequest($repository, $devBranch, $mainBranch, "Automatic merge '$devBranch' into '$mainBranch'", true);
        $mergeRequestManager->mergeMergeRequest($mergeRequestFromDevToMain);

        $mergeRequestFromMainToDev = $mergeRequestManager->findOrCreateMergeRequest($repository, $mainBranch, $devBranch, "Automatic merge '$mainBranch' into '$devBranch'", false);
        $mergeRequestManager->mergeMergeRequest($mergeRequestFromMainToDev);
    }

    private function createRelease(
        Repository $repository,
        PreparedVersionDTO $preparedVersionDTO,
        Version $releasedVersion,
        BasedCodeRepositoryManagerInterface $repositoryManager,
    ): string
    {
        $releaseCode = $preparedVersionDTO->versionCode;
        $createBranchForNewVersion = $repository->isCreateBranchForNewVersion();
        $releaseContent = $this->markdownPatchNoteExporter->exportVersion($releasedVersion, PatchNoteExportTargetEnum::DEV);
        $tagCommitSha = $repositoryManager->createTag($repository, $repository->getMainBranch(), $releaseCode, $releaseContent);

        if ($preparedVersionDTO->releaseVersion) {
            $repositoryManager->createRelease($repository, $releaseCode, $releaseCode, $releaseContent);
        }

        if ($createBranchForNewVersion) {
            $this->createBranchForNewVersion($repository, $preparedVersionDTO, $repositoryManager);
        }

        return $tagCommitSha;
    }

    private function createBranchForNewVersion(
        Repository $repository,
        PreparedVersionDTO $preparedVersionDTO,
        BasedCodeRepositoryManagerInterface $repositoryManager,
    ): void
    {
        $releaseCode = $preparedVersionDTO->versionCode;
        $mainBranch = $repository->getMainBranch();

        $repositoryManager->createBranchIfNotExists($repository, $mainBranch, $releaseCode);
    }
}