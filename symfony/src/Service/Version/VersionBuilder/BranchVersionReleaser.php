<?php
declare(strict_types=1);

namespace App\Service\Version\VersionBuilder;

use App\ApiResource\BasedCode;
use App\ApiResource\UpdateTypeEnum;
use App\ApiResource\VersionTypeEnum;
use App\DTO\Version\BranchPreparedVersionDTO;
use App\DTO\Version\ExistingVersionPreparedVersionDTO;
use App\DTO\Version\NewVersionPreparedVersionDTO;
use App\DTO\Version\PreparedVersionDTO;
use App\Entity\Repository;
use App\Entity\Version;
use App\Service\BasedCode\BasedCodeRepository;
use App\Service\BasedCode\Handler\BasedCodeMergeRequestManagerInterface;
use App\Service\BasedCode\Handler\BasedCodeRepositoryManagerInterface;
use App\Service\PatchNoteSection\Exporter\MarkdownPatchNoteExporter;
use App\Service\PatchNoteSection\Exporter\PatchNoteExportTargetEnum;
use App\Service\Repository\MergeRequestSynchronizer;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use function Symfony\Component\String\u;

readonly class BranchVersionReleaser implements UpdateTypeVersionReleaserInterface
{
    public function getSupportedUpdateType(): UpdateTypeEnum
    {
        return UpdateTypeEnum::BRANCH;
    }

    public function release(PreparedVersionDTO $preparedVersion, BasedCodeRepositoryManagerInterface $repositoryManager, BasedCodeMergeRequestManagerInterface $mergeRequestManager,): Version
    {
        // TODO: Implement release() method.
        throw new Exception();
    }
}