<?php

namespace App\Service\Version\VersionBuilder;

use App\ApiResource\UpdateTypeEnum;
use App\DTO\Version\PreparedVersionDTO;
use App\Entity\Version;
use App\Service\BasedCode\Handler\BasedCodeMergeRequestManagerInterface;
use App\Service\BasedCode\Handler\BasedCodeRepositoryManagerInterface;
use Exception;

readonly class ExistingVersionReleaser implements UpdateTypeVersionReleaserInterface
{
    public function getSupportedUpdateType(): UpdateTypeEnum
    {
        return UpdateTypeEnum::EXISTING_VERSION;
    }

    public function release(PreparedVersionDTO $preparedVersion, BasedCodeRepositoryManagerInterface $repositoryManager, BasedCodeMergeRequestManagerInterface $mergeRequestManager,): Version
    {
        // TODO: Implement release() method.
        throw new Exception();
    }
}