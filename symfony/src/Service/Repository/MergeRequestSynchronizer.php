<?php

namespace App\Service\Repository;

use App\ApiResource\BasedCode;
use App\Entity\MergeRequest;
use App\Entity\Repository;
use App\Repository\MergeRequestRepository;
use App\Service\BasedCode\BasedCodeRepository;
use App\Service\BasedCode\Handler\BasedCodeMergeRequestManagerInterface;
use App\Service\MergeRequest\MergeRequestUpdater;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Attribute\WithMonologChannel;
use Symfony\Component\HttpFoundation\Request;

#[WithMonologChannel('based_code.synchronizer')]
final readonly class MergeRequestSynchronizer
{
    public function __construct(
        private BasedCodeRepository $basedCodeRepository,
        private EntityManagerInterface $entityManager,
        private MergeRequestUpdater $mergeRequestUpdater,
        private MergeRequestRepository $mergeRequestRepository,
    )
    {
    }

    public function syncFromRequest(Request $request, BasedCode $basedCode): MergeRequest
    {
        return $basedCode->getBasedCodeHandler()->getMergeRequestSynchronizerFromWebhook()->sync($request);
    }

    public function syncRepository(Repository $repository, bool $syncMerged, bool $syncNotMerged): void
    {
        $basedCode = $this->basedCodeRepository->findForRepository($repository);

        if (null === $basedCode) {
            return;
        }

        $repositoryProvider = $basedCode->getBasedCodeHandler()->getRepositoryManager();

        $processedMergeRequestCodes = [];

        if ($syncMerged) {
            $mergedMergeRequests = $repositoryProvider->findMergeRequests($repository, true);
            $processedMergeRequestCodes = [
                ...$processedMergeRequestCodes,
                ...$this->processMergeRequests($mergedMergeRequests),
            ];
        }

        if ($syncNotMerged) {
            $notMergedMergeRequests = $repositoryProvider->findMergeRequests($repository, false);
            $processedMergeRequestCodes = [
                ...$processedMergeRequestCodes,
                ...$this->processMergeRequests($notMergedMergeRequests),
            ];
        }

        $mergeRequestProvider = $basedCode->getBasedCodeHandler()->getMergeRequestManager();

        $this->processNotSyncedMergeRequests($repository, $processedMergeRequestCodes, $mergeRequestProvider);

        $this->entityManager->flush();
    }

    /**
     * @param array<MergeRequest> $mergeRequests
     *
     * @return array<string>
     */
    private function processMergeRequests(array $mergeRequests): array
    {
        $codes = [];

        foreach ($mergeRequests as $mergeRequest) {
            $code = $mergeRequest->getCode();
            $existingMergeRequest = $this->mergeRequestRepository->findOneBy(['code' => $code]);

            if (null === $existingMergeRequest) {
                $existingMergeRequest = $mergeRequest;
            } else {
                $this->mergeRequestUpdater->updateFromRemote($existingMergeRequest, $mergeRequest);
            }

            $version = $mergeRequest->getVersion();

            if ($version) {
                $this->entityManager->persist($version);
            }

            $this->entityManager->persist($existingMergeRequest);
            $codes[] = $code;
        }

        return $codes;
    }

    /**
     * @param array<string> $processedMergeRequestCodes
     */
    private function processNotSyncedMergeRequests(Repository $repository, array $processedMergeRequestCodes, BasedCodeMergeRequestManagerInterface $mergeRequestProvider): void
    {
        $notSyncedMergeRequests = $this->mergeRequestRepository->findRepositoryMergeRequests(
            repository: $repository,
            isMerged: false,
            isVersioned: false,
            excludedCodes: $processedMergeRequestCodes
        );

        foreach ($notSyncedMergeRequests as $mergeRequest) {
            $remoteMergeRequest = $mergeRequestProvider->findOne(
                $repository->getHost(),
                $repository->getToken(),
                $mergeRequest->getCode()
            );

            if (!$remoteMergeRequest->targetBranchIsBranchUsedToVersion()) {
                continue;
            }

            $this->mergeRequestUpdater->updateFromRemote($mergeRequest, $remoteMergeRequest);
            $this->entityManager->persist($mergeRequest);
        }
    }
}