<?php

namespace App\Service\Repository;

use App\ApiResource\VersionTypeEnum;
use App\DTO\Version\NewVersionPreparedVersionDTO;
use App\DTO\Version\PreparedVersionDTO;
use App\Entity\Repository;

class VersionPreparer
{
    public function build(Repository $repository, ?Repository $parentRepository = null): PreparedVersionDTO
    {
        $release = $this->instantiateRelease($repository, $parentRepository);

        $release->repository = $repository;

        foreach ($repository->getSubModules() as $submodule) {
            $release->submoduleReleases[] = $this->build($submodule->getChild(), $repository);
            // TODO : rajouter la vérification des versions
            // TODO : rajouter la vérification des mrs
        }

        return $release;
    }

    private function instantiateRelease(Repository $repository, ?Repository $parentRepository): PreparedVersionDTO
    {
        $newCode = '';

        $decomposedLatestCode = $repository->getLatestVersion()?->getDecomposedCode();

        if (!empty($decomposedLatestCode)) {
            $decomposedNewCode = [...$decomposedLatestCode];
            $decomposedNewCode['PATCH']++;

            $newCode = implode('.', $decomposedNewCode);
        }

        $defaultVersionDTO = new NewVersionPreparedVersionDTO(
            versionType: VersionTypeEnum::PATCH,
            versionCode: $newCode,
            releaseVersion: false
        );

        if (!$parentRepository) {
            return $defaultVersionDTO;
        }

        $latestVersion = $repository->getLatestVersion();

        if (!$latestVersion) {
            return $defaultVersionDTO;
        }

        $lastSubmoduleVersion = $latestVersion->getVersionForSubmodule($repository);

        if (!$lastSubmoduleVersion) {
            return $defaultVersionDTO;
        }

        return $lastSubmoduleVersion->createSubmodulepreparedVersion();
    }
}