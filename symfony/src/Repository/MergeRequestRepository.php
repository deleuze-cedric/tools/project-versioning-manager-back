<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\MergeRequest;
use App\Entity\Repository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MergeRequest>
 */
class MergeRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MergeRequest::class);
    }

    /**
     * @param array<string> $excludedCodes
     * @return array<MergeRequest>
     */
    public function findRepositoryMergeRequests(Repository $repository, ?bool $isMerged = null, ?bool $isVersioned = null, array $excludedCodes = []): array
    {
        $query = $this->createQueryBuilder('mr')
            ->innerJoin('mr.version', 'v')
            ->andWhere('v.repository = :repository')
            ->setParameter('repository', $repository);

        if (count($excludedCodes) > 0) {
            $query->andWhere('mr.code NOT IN (:excludedCodes)')
                ->setParameter('excludedCodes', $excludedCodes);
        }

        if (null !== $isMerged) {
            $query->andWhere('mr.mergedAt '.($isMerged ? 'IS NOT NULL' : 'IS NULL'));
        }

        if (null !== $isVersioned) {
            $query->andWhere('v.versionedAt '.($isVersioned ? 'IS NOT NULL' : 'IS NULL'));
        }

        return $query->getQuery()->getResult();
    }

    public function findOneByCode(string $code): ?MergeRequest
    {
        return $this->createQueryBuilder('mr')
            ->where('mr.code = :code')
            ->setParameter('code', $code)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
