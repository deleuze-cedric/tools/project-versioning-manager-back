<?php

namespace App\State\BasedCode;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\ApiResource\BasedCode;
use App\Service\BasedCode\BasedCodeRepository;

final readonly class BasedCodeProvider implements ProviderInterface
{
    public function __construct(
        private BasedCodeRepository $basedCodeRepository
    )
    {
    }

    /**
     * @return BasedCode|array<BasedCode>|null
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): BasedCode|array|null
    {
        $id = $uriVariables['id'] ?? null;

        if ($id) {
            return $this->basedCodeRepository->findById($id);
        }

        return $this->basedCodeRepository->find();
    }
}
