<?php

namespace App\Tools;

class PathMerger
{
    public function merge(string $firstPath, string $secondPath): string {
        $finalPath = explode('/', $firstPath);

        foreach (explode('/', $secondPath) as $segment) {
            switch($segment) {
                case '.':
                    continue 2;
                case '..':
                    array_pop($finalPath);
                    break;
                default:
                    $finalPath[] = $segment;
                    break;
            }
        }

        return implode('/', $finalPath);
    }
}