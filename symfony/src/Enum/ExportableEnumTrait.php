<?php
declare(strict_types=1);

namespace App\Enum;

use ApiPlatform\Metadata\Operation;
use App\DTO\Version\PreparedVersionDTO;
use BackedEnum;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;

trait ExportableEnumTrait
{
    #[Groups([
        self::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    public function getId(): string
    {
        return $this->name;
    }

    #[Groups([
        self::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    public function getLabel(): string
    {
        if ($this instanceof BackedEnum) {
            return (string)$this->value;
        }

        return '';
    }

    /**
     * @return array<self>
     */
    public static function getCases(): array
    {
        return self::cases();
    }


    public static function getCase(Operation $operation, array $uriVariables): self
    {
        $name = $uriVariables['id'] ?? null;

        return constant(self::class."::$name");
    }
}