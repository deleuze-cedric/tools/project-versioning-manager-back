<?php

namespace App\Enum;

use ApiPlatform\Metadata\Operation;

interface ExportableEnumInterface
{
    public const string API_READ_GROUP = self::class.':api:read';

    public function getId(): string;

    public function getLabel(): string;

    /** @return array<self> * */
    public static function getCases(): array;

    public static function getCase(Operation $operation, array $uriVariables): self;
}