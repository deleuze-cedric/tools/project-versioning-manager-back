<?php

namespace App\Factory;

use App\Entity\Repository;
use App\Repository\RepositoryRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Repository>
 *
 * @method        Repository|Proxy                     create(array|callable $attributes = [])
 * @method static Repository|Proxy                     createOne(array $attributes = [])
 * @method static Repository|Proxy                     find(object|array|mixed $criteria)
 * @method static Repository|Proxy                     findOrCreate(array $attributes)
 * @method static Repository|Proxy                     first(string $sortedField = 'id')
 * @method static Repository|Proxy                     last(string $sortedField = 'id')
 * @method static Repository|Proxy                     random(array $attributes = [])
 * @method static Repository|Proxy                     randomOrCreate(array $attributes = [])
 * @method static RepositoryRepository|RepositoryProxy repository()
 * @method static Repository[]|Proxy[]                 all()
 * @method static Repository[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static Repository[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static Repository[]|Proxy[]                 findBy(array $attributes)
 * @method static Repository[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static Repository[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 */
final class RepositoryFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'basedCodeIdentifier' => self::faker()->text(10),
            'code' => self::faker()->text(255),
            'devBranch' => self::faker()->text(10),
            'host' => self::faker()->text(30),
            'link' => self::faker()->text(255),
            'mainBranch' => self::faker()->text(10),
            'name' => self::faker()->text(50),
            'token' => self::faker()->text(255),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Repository $repository): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Repository::class;
    }
}
