<?php

namespace App\Controller\Enum;

use App\ApiResource\UpdateTypeEnum;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetUpdateTypesController extends AbstractController
{
    /**
     * @return array<UpdateTypeEnum>
     */
    public function __invoke(): array
    {
        return UpdateTypeEnum::cases();
    }
}