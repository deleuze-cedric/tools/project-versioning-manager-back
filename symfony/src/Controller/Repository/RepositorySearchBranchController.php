<?php

namespace App\Controller\Repository;

use App\ApiResource\BasedCode;
use App\DTO\Repository\Branch;
use App\Entity\Repository;
use App\Repository\RepositoryRepository;
use App\Service\BasedCode\BasedCodeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;

class RepositorySearchBranchController extends AbstractController
{
    /**
     * @return iterable<Branch>
     */
    public function __invoke(
        BasedCode $data,
        string $code,
        RepositoryRepository $repositoryRepository,
        BasedCodeRepository $basedCodeRepository,
        #[MapQueryParameter] string $search = '',
    ): iterable
    {
        $repository = (new Repository())
            ->setCode($code)
            ->setBasedCodeIdentifier($data->getId());

        return $data
            ->getBasedCodeHandler()
            ->getRepositoryManager()
            ->searchBranch($repository, $search);
    }
}