<?php

namespace App\Controller\Repository;

use App\DTO\Version\PreparedVersionDTO;
use App\Entity\Repository;
use App\Service\Version\VersionBuilder\VersionReleaser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CreateNewReleaseController extends AbstractController
{
    public function __invoke(Repository $repository, PreparedVersionDTO $data, VersionReleaser $repositoryVersionBuilder): Repository
    {
        $repositoryVersionBuilder->releaseVersion($data);

        return $repository;
    }
}