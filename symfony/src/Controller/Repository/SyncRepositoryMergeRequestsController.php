<?php

namespace App\Controller\Repository;

use App\Entity\Repository;
use App\Service\Repository\MergeRequestSynchronizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SyncRepositoryMergeRequestsController extends AbstractController
{
    public function __construct(
        private readonly MergeRequestSynchronizer $mergeRequestSynchronizer
    )
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(Repository $data): JsonResponse
    {
        $this->mergeRequestSynchronizer->syncRepository($data, true, true);

        return new JsonResponse(status: Response::HTTP_NO_CONTENT);
    }
}
