<?php
declare(strict_types=1);

namespace App\Controller\Repository;

use App\DTO\Version\PreparedVersionDTO;
use App\Entity\Repository;
use App\Service\Repository\VersionPreparer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrepareVersionController extends AbstractController
{
    public function __invoke(Repository $repository, VersionPreparer $versionPreparer): PreparedVersionDTO
    {
        return $versionPreparer->build($repository);
    }
}