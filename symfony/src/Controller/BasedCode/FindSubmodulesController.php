<?php

namespace App\Controller\BasedCode;

use App\ApiResource\BasedCode;
use App\Repository\RepositoryRepository;
use App\Service\BasedCode\SubmoduleReader\SubmoduleListDTO;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

#[AsController]
class FindSubmodulesController extends AbstractBasedCodeController
{
    public function __construct(
        private readonly RepositoryRepository $repositoryRepository
    )
    {
    }

    public function __invoke(BasedCode $data, string $code): SubmoduleListDTO
    {
        $repository = $this->repositoryRepository->findOneBy(['code' => $code]);

        if (!$repository) {
            throw new NotFoundHttpException('Repository with code "'.$code.'" not found');
        }

        return $data->getBasedCodeHandler()->getRepositoryManager()->findSubModules($repository);
    }
}
