<?php

namespace App\Controller\BasedCode;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Contracts\Service\Attribute\Required;

class AbstractBasedCodeController extends AbstractController
{
    private readonly RequestStack $requestStack;

    #[Required]
    public function setRequestStack(RequestStack $requestStack): void {
        $this->requestStack = $requestStack;
    }

    protected function getHost(): ?string
    {
        $currentRequest = $this->requestStack->getCurrentRequest();
        return $currentRequest->get('host');
    }

    protected function getToken(bool $throw = true): ?string
    {
        $currentRequest = $this->requestStack->getCurrentRequest();
        $token = $currentRequest->get('token');

        if (empty($token) && $throw) {
            throw new BadRequestHttpException('The access token must be defined');
        }

        return $token;
    }
}
