<?php

namespace App\Controller\BasedCode;

use App\ApiResource\BasedCode;
use App\Entity\MergeRequest;
use App\Service\Repository\MergeRequestSynchronizer;
use Symfony\Component\HttpFoundation\Request;

class SyncMergeRequestFromWebhookController extends AbstractBasedCodeController
{
    public function __construct(
        private readonly MergeRequestSynchronizer $mergeRequestSynchronizer
    )
    {
    }

    public function __invoke(BasedCode $data, Request $request): MergeRequest
    {
        return $this->mergeRequestSynchronizer->syncFromRequest($request, $data);
    }
}
