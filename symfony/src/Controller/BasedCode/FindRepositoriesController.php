<?php

namespace App\Controller\BasedCode;

use App\ApiResource\BasedCode;
use App\Entity\Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

#[AsController]
class FindRepositoriesController extends AbstractBasedCodeController
{
    /**
     * @return array<Repository>
     */
    public function __invoke(
        BasedCode $data,
        #[MapQueryParameter] string $search = ''
    ): array
    {
        $auth = $this->getToken();
        $host = $this->getHost();
        return $data->getBasedCodeHandler()->getRepositoryManager()->find($host, $auth, $search);
    }
}
