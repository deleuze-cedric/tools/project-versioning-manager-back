<?php

namespace App\Listener\Repository;

use App\Entity\Repository;
use App\Service\Repository\MergeRequestSynchronizer;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\PostPersist;

#[AsEntityListener(event: Events::postPersist, method: 'postPersist', entity: Repository::class)]
final readonly class ImportRepositoryMergeRequestsListener
{
    public function __construct(
        private MergeRequestSynchronizer $mergeRequestSynchronizer
    )
    {
    }

    public function postPersist(Repository $repository): void
    {
        $this->mergeRequestSynchronizer->syncRepository($repository, true, true);
    }

}