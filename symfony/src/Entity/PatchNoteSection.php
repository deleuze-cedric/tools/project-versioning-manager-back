<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\PatchNoteSectionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints\Length;

#[ORM\Entity(repositoryClass: PatchNoteSectionRepository::class)]
class PatchNoteSection
{
    // TODO : rajouter la validation
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([
        MergeRequest::API_READ_GROUP,
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 15)]
    #[Groups([
        Repository::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        MergeRequest::API_WRITE_GROUP,
    ])]
    private ?string $code = null;

    #[ORM\Column(length: 30)]
    #[Groups([
        Repository::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        MergeRequest::API_WRITE_GROUP,
    ])]
    private ?string $name = null;

    #[ORM\Column(type: Types::JSON)]
    #[Groups([
        Repository::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        MergeRequest::API_WRITE_GROUP,
    ])]
    private array $content = [];

    #[ORM\Column]
    #[Groups([
        Repository::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        MergeRequest::API_WRITE_GROUP,
    ])]
    private ?bool $required = null;

    #[ORM\ManyToOne(inversedBy: 'patchNoteSections')]
    private ?MergeRequest $mergeRequest = null;

    #[ORM\ManyToOne(inversedBy: 'patchNoteSections')]
    private ?Version $version = null;

    #[ORM\ManyToOne(inversedBy: 'patchNoteSections')]
    private ?Commit $commit = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(array $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function isRequired(): ?bool
    {
        return $this->required;
    }

    public function setRequired(bool $required): static
    {
        $this->required = $required;

        return $this;
    }

    public function getMergeRequest(): ?MergeRequest
    {
        return $this->mergeRequest;
    }

    public function setMergeRequest(?MergeRequest $mergeRequest): static
    {
        $this->mergeRequest = $mergeRequest;

        return $this;
    }

    public function getVersion(): ?Version
    {
        return $this->version;
    }

    public function setVersion(?Version $version): static
    {
        $this->version = $version;

        return $this;
    }

    public function getCommit(): ?Commit
    {
        return $this->commit;
    }

    public function setCommit(?Commit $commit): static
    {
        $this->commit = $commit;

        return $this;
    }

    public function isEmpty(): bool
    {
        return count($this->content) < 1;
    }

    public function addContentLine(string $contentLine): self
    {
        $this->content[] = $contentLine;

        return $this;
    }
}
