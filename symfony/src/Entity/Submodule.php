<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\ApiResource\BasedCode;
use App\Repository\SubmoduleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\MaxDepth;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

#[ApiResource]
#[ORM\Entity(repositoryClass: SubmoduleRepository::class)]
class Submodule
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[ApiProperty(identifier: true)]
    #[Groups([
        BasedCode::API_READ_GROUP,
        Repository::API_WRITE_GROUP
    ])]
    private ?string $code = null;

    #[ORM\ManyToOne(inversedBy: 'submodules')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull(message: 'Le dépôt source doit être renseigné')]
    #[Groups([
        BasedCode::API_READ_GROUP,
        Repository::API_READ_GROUP,
        Repository::API_WRITE_GROUP,
    ])]
    #[ApiProperty(readableLink: false, writableLink: false)]
    private Repository $parent;

    #[ORM\ManyToOne(inversedBy: 'parentRepositories')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull(message: 'Le dépôt cible doit être renseigné')]
    #[Groups([
        BasedCode::API_READ_GROUP,
        Repository::API_READ_GROUP,
        Repository::API_WRITE_GROUP,
    ])]
    #[ApiProperty(readableLink: false, writableLink: false)]
    private Repository $child;

    #[ORM\Column(length: 255)]
    #[Length(min: 1, max: 255, minMessage: 'Le lien doit contenir au moins {{ limit }} caractères', maxMessage: 'Le lien doit contenir au maximum {{ limit }} caractères')]
    #[NotNull(message: 'Le lien doit être renseigné')]
    #[Groups([
        Repository::API_READ_GROUP,
        Repository::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
    ])]
    private string $link;

    #[ORM\Column(length: 255)]
    #[NotNull(message: 'Le chemin doit être renseigné')]
    #[Length(min: 1, max: 255, minMessage: 'Le chemin doit contenir au moins {{ limit }} caractères', maxMessage: 'Le chemin doit contenir au maximum {{ limit }} caractères')]
    #[Groups([
        Repository::API_READ_GROUP,
        Repository::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
    ])]
    private string $path;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getParent(): ?Repository
    {
        return $this->parent;
    }

    public function setParent(?Repository $parent): static
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChild(): ?Repository
    {
        return $this->child;
    }

    public function setChild(?Repository $child): static
    {
        $this->child = $child;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): static
    {
        $this->link = $link;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): static
    {
        $this->path = $path;

        return $this;
    }

    public function setId(int $id): self
    {
        $this->id = null;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }
}
