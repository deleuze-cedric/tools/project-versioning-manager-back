<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CommitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommitRepository::class)]
#[ORM\UniqueConstraint(name: 'commit_unique_code', fields: ['code'])]
#[ORM\UniqueConstraint(name: 'commit_unique_link', fields: ['link'])]
#[ApiResource(
    operations: []
)]
class Commit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[ApiProperty(identifier: true)]
    private ?string $code = null;

    #[ORM\Column(length: 255)]
    private ?string $link = null;

    /**
     * @var Collection<int, PatchNoteSection>
     */
    #[ORM\OneToMany(targetEntity: PatchNoteSection::class, mappedBy: 'commit')]
    private Collection $patchNoteSections;

    #[ORM\ManyToOne(inversedBy: 'commits')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Version $version = null;

    public function __construct()
    {
        $this->patchNoteSections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): static
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection<int, PatchNoteSection>
     */
    public function getPatchNoteSections(): Collection
    {
        return $this->patchNoteSections;
    }

    public function addPatchNoteSection(PatchNoteSection $patchNoteSection): static
    {
        if (!$this->patchNoteSections->contains($patchNoteSection)) {
            $this->patchNoteSections->add($patchNoteSection);
            $patchNoteSection->setCommit($this);
        }

        return $this;
    }

    public function removePatchNoteSection(PatchNoteSection $patchNoteSection): static
    {
        if ($this->patchNoteSections->removeElement($patchNoteSection)) {
            // set the owning side to null (unless already changed)
            if ($patchNoteSection->getCommit() === $this) {
                $patchNoteSection->setCommit(null);
            }
        }

        return $this;
    }

    public function getVersion(): ?Version
    {
        return $this->version;
    }

    public function setVersion(?Version $version): static
    {
        $this->version = $version;

        return $this;
    }
}
