<?php

namespace App\Entity;

use App\DTO\Version\BranchPreparedVersionDTO;
use App\DTO\Version\NewVersionPreparedVersionDTO;
use App\DTO\Version\PreparedVersionDTO;
use App\Repository\SubmoduleBranchVersionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(SubmoduleBranchVersionRepository::class)]
class SubmoduleBranchVersion extends SubmoduleVersion
{
    #[ORM\Column(length: 100)]
    private ?string $branch = null;

    #[ORM\Column]
    private ?bool $pullLatestChanges = null;

    public function getBranch(): ?string
    {
        return $this->branch;
    }

    public function setBranch(string $branch): static
    {
        $this->branch = $branch;

        return $this;
    }

    public function isPullLatestChanges(): ?bool
    {
        return $this->pullLatestChanges;
    }

    public function setPullLatestChanges(bool $pullLatestChanges): static
    {
        $this->pullLatestChanges = $pullLatestChanges;

        return $this;
    }

    public function createSubmodulepreparedVersion(): PreparedVersionDTO
    {
        return new BranchPreparedVersionDTO(
            branch: $this->branch,
            pullLatestChanges: $this->pullLatestChanges
        );
    }
}
