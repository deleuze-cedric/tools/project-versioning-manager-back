<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use App\Filter\MergeRequest\MergeRequestGlobalSearchFilter;
use App\Filter\MergeRequest\MergeRequestNotInMergeRequestDependenciesFilter;
use App\Filter\Version\RepositoryFromVersionFilter;
use App\Repository\MergeRequestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: MergeRequestRepository::class)]
#[ORM\UniqueConstraint('merge_request_unique_code', ['code'])]
#[ORM\UniqueConstraint('merge_request_unique_link', ['link'])]
#[ApiResource(
    normalizationContext: [
        'groups' => self::API_READ_GROUP
    ],
    denormalizationContext: [
        'groups' => self::API_WRITE_GROUP
    ],
)]
#[GetCollection()]
#[Patch()]
#[ApiFilter(RepositoryFromVersionFilter::class, properties: ['repository'])]
#[ApiFilter(MergeRequestGlobalSearchFilter::class, properties: ['search'])]
#[ApiFilter(MergeRequestNotInMergeRequestDependenciesFilter::class, properties: ['excludes_merge_request_dependencies'])]
# TODO : rajouter des vérifs sur les statuts de la mr (si déjà merge => non fermable, si fermée => non mergeable)
# TODO : rajouter une vérif sur le nom des branches
class MergeRequest
{
    // TODO : rajouter la validation
    public const string API_READ_GROUP = self::class . ':read';
    public const string API_WRITE_GROUP = self::class . ':write';
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[ApiProperty(identifier: true)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private ?string $code = null;


    #[ORM\Column(length: 150)]
    #[Groups([
        self::API_READ_GROUP,
    ])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private ?string $link = null;

    /**
     * @var Collection<int, PatchNoteSection>
     */
    #[ORM\OneToMany(targetEntity: PatchNoteSection::class, mappedBy: 'mergeRequest', cascade: ['persist', 'remove'])]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private Collection $patchNoteSections;

    #[ORM\Column(nullable: true)]
    #[ApiFilter(ExistsFilter::class)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private ?\DateTimeImmutable $mergedAt = null;

    #[ORM\Column(nullable: true)]
    #[ApiFilter(ExistsFilter::class)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private ?\DateTimeImmutable $processedAt = null;

    #[ORM\Column(nullable: true)]
    #[ApiFilter(ExistsFilter::class)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private ?\DateTimeImmutable $closedAt = null;

    #[ORM\ManyToOne(inversedBy: 'mergeRequests')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([
        self::API_READ_GROUP,
    ])]
    private ?Version $version;

    /**
     * @var Collection<int, self>
     */
    #[ORM\ManyToMany(targetEntity: self::class, inversedBy: 'requiredByMergeRequests')]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private Collection $mergeRequestDependencies;

    /**
     * @var Collection<int, self>
     */
    #[ORM\ManyToMany(targetEntity: self::class, mappedBy: 'mergeRequestDependencies')]
    #[Groups([
        self::API_READ_GROUP,
    ])]
    private Collection $requiredByMergeRequests;

    /**
     * @var Collection<int, Version>
     */
    #[ORM\ManyToMany(targetEntity: Version::class, inversedBy: 'requiredByMergeRequests')]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private Collection $versionDependencies;

    #[ORM\Column(length: 100, nullable: false)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private ?string $sourceBranch = null;

    #[ORM\Column(length: 100, nullable: false)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private ?string $targetBranch = null;

    /**
     * @var Collection<int, WorkingItem>
     */
    #[ORM\ManyToMany(targetEntity: WorkingItem::class, inversedBy: 'mergeRequests')]
    private Collection $workingItems;

    public function __construct()
    {
        $this->patchNoteSections = new ArrayCollection();
        $this->mergeRequestDependencies = new ArrayCollection();
        $this->requiredByMergeRequests = new ArrayCollection();
        $this->versionDependencies = new ArrayCollection();
        $this->workingItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): static
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection<int, PatchNoteSection>
     */
    public function getPatchNoteSections(): Collection
    {
        return $this->patchNoteSections;
    }

    /**
     * @param Collection<PatchNoteSection> $patchNoteSections
     */
    public function setPatchNoteSections(Collection $patchNoteSections): self
    {
        $this->patchNoteSections = $patchNoteSections;

        return $this;
    }

    public function addPatchNoteSection(PatchNoteSection $patchNoteSection): static
    {
        if (!$this->patchNoteSections->contains($patchNoteSection)) {
            $this->patchNoteSections->add($patchNoteSection);
            $patchNoteSection->setMergeRequest($this);
        }

        return $this;
    }

    public function removePatchNoteSection(PatchNoteSection $patchNoteSection): static
    {
        if ($this->patchNoteSections->removeElement($patchNoteSection)) {
            // set the owning side to null (unless already changed)
            if ($patchNoteSection->getMergeRequest() === $this) {
                $patchNoteSection->setMergeRequest(null);
            }
        }

        return $this;
    }

    public function getMergedAt(): ?\DateTimeImmutable
    {
        return $this->mergedAt;
    }

    public function setMergedAt(?\DateTimeImmutable $mergedAt): static
    {
        $this->mergedAt ??= $mergedAt;

        return $this;
    }

    public function getProcessedAt(): ?\DateTimeImmutable
    {
        return $this->processedAt;
    }

    public function setProcessedAt(?\DateTimeImmutable $processedAt): static
    {
        $this->processedAt = $processedAt;

        return $this;
    }

    public function getVersion(): ?Version
    {
        return $this->version;
    }

    public function setVersion(?Version $version): static
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getMergeRequestDependencies(): Collection
    {
        return $this->mergeRequestDependencies;
    }

    public function addMergeRequestDependency(self $mergeRequestDependency): static
    {
        if (!$this->mergeRequestDependencies->contains($mergeRequestDependency)) {
            $this->mergeRequestDependencies->add($mergeRequestDependency);
        }

        return $this;
    }

    public function removeMergeRequestDependency(self $mergeRequestDependency): static
    {
        $this->mergeRequestDependencies->removeElement($mergeRequestDependency);

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getRequiredByMergeRequests(): Collection
    {
        return $this->requiredByMergeRequests;
    }

    public function addRequiredByMergeRequest(self $requiredByMergeRequest): static
    {
        if (!$this->requiredByMergeRequests->contains($requiredByMergeRequest)) {
            $this->requiredByMergeRequests->add($requiredByMergeRequest);
            $requiredByMergeRequest->addMergeRequestDependency($this);
        }

        return $this;
    }

    public function removeRequiredByMergeRequest(self $requiredByMergeRequest): static
    {
        if ($this->requiredByMergeRequests->removeElement($requiredByMergeRequest)) {
            $requiredByMergeRequest->removeMergeRequestDependency($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Version>
     */
    public function getVersionDependencies(): Collection
    {
        return $this->versionDependencies;
    }

    public function addVersionDependency(Version $versionDependency): static
    {
        if (!$this->versionDependencies->contains($versionDependency)) {
            $this->versionDependencies->add($versionDependency);
        }

        return $this;
    }

    public function removeVersionDependency(Version $versionDependency): static
    {
        $this->versionDependencies->removeElement($versionDependency);

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getClosedAt(): ?\DateTimeImmutable
    {
        return $this->closedAt;
    }

    public function setClosedAt(?\DateTimeImmutable $closedAt): static
    {
        $this->closedAt = $closedAt;

        return $this;
    }

    public function getSourceBranch(): ?string
    {
        return $this->sourceBranch;
    }

    public function setSourceBranch(string $sourceBranch): static
    {
        $this->sourceBranch = $sourceBranch;

        return $this;
    }

    public function getTargetBranch(): ?string
    {
        return $this->targetBranch;
    }

    public function setTargetBranch(string $targetBranch): static
    {
        $this->targetBranch = $targetBranch;

        return $this;
    }

    public function getRepository(): Repository {
        return $this->getVersion()->getRepository();
    }

    public function targetBranchIsBranchUsedToVersion(): bool {
        return $this->getRepository()->isBranchUsedToVersion($this->targetBranch);
    }

    public function isDevMergeRequest(): bool
    {
        return $this->getTargetBranch() === $this->getRepository()->getDevBranch();
    }

    public function isMainMergeRequest(): bool
    {
        return $this->getTargetBranch() === $this->getRepository()->getMainBranch();
    }

    public function isMerged(): bool
    {
        return null !== $this->getMergedAt();
    }

    /**
     * @return Collection<int, WorkingItem>
     */
    public function getWorkingItems(): Collection
    {
        return $this->workingItems;
    }

    public function addWorkingItem(WorkingItem $workingItem): static
    {
        if (!$this->workingItems->contains($workingItem)) {
            $this->workingItems->add($workingItem);
        }

        return $this;
    }

    public function removeWorkingItem(WorkingItem $workingItem): static
    {
        $this->workingItems->removeElement($workingItem);

        return $this;
    }
}
