<?php

namespace App\Entity;

use App\DTO\Version\ExistingVersionPreparedVersionDTO;
use App\DTO\Version\NewVersionPreparedVersionDTO;
use App\DTO\Version\PreparedVersionDTO;
use App\Repository\SubmoduleExistingVersionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(SubmoduleExistingVersionRepository::class)]
class SubmoduleExistingVersion extends SubmoduleVersion
{
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Version $targetVersion = null;

    public function getTargetVersion(): ?Version
    {
        return $this->targetVersion;
    }

    public function setTargetVersion(?Version $targetVersion): static
    {
        $this->targetVersion = $targetVersion;

        return $this;
    }

    public function createSubmodulepreparedVersion(): PreparedVersionDTO
    {
        return new ExistingVersionPreparedVersionDTO(
            targetVersion: $this->targetVersion
        );
    }
}
