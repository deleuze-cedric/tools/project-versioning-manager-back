<?php

namespace App\Entity;

use App\Repository\WorkingItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: WorkingItemRepository::class)]
class WorkingItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'uuid')]
    private ?Uuid $uuid = null;

    #[ORM\Column(length: 50)]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\ManyToOne]
    private ?Client $client = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $source = null;

    #[ORM\Column(length: 255)]
    private ?string $link = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'childrenItems')]
    private ?self $parentItem = null;

    /**
     * @var Collection<int, self>
     */
    #[ORM\OneToMany(targetEntity: self::class, mappedBy: 'parentItem')]
    private Collection $childrenItems;

    #[ORM\Column(nullable: true)]
    private ?int $priorityScore = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?WorkingItemType $type = null;

    #[ORM\ManyToOne]
    private ?Worker $supportBy = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $supportedAt = null;

    #[ORM\ManyToOne]
    private ?Worker $escalatedBy = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $escalatedAt = null;

    #[ORM\ManyToOne(inversedBy: 'plannedWorkingItems')]
    private ?Version $plannedVersion = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $plannedAt = null;

    #[ORM\ManyToOne(inversedBy: 'actualWorkingItems')]
    private ?Version $actualVersion = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $versionedAt = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $realizedAt = null;

    #[ORM\ManyToOne]
    private ?Worker $realizedBy = null;

    /**
     * @var Collection<int, MergeRequest>
     */
    #[ORM\ManyToMany(targetEntity: MergeRequest::class, mappedBy: 'workingItems')]
    private Collection $mergeRequests;

    public function __construct()
    {
        $this->childrenItems = new ArrayCollection();
        $this->mergeRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): static
    {
        $this->source = $source;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): static
    {
        $this->client = $client;

        return $this;
    }

    public function getParentItem(): ?self
    {
        return $this->parentItem;
    }

    public function setParentItem(?self $parentItem): static
    {
        $this->parentItem = $parentItem;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getChildrenItems(): Collection
    {
        return $this->childrenItems;
    }

    public function addChildrenItem(self $childrenItem): static
    {
        if (!$this->childrenItems->contains($childrenItem)) {
            $this->childrenItems->add($childrenItem);
            $childrenItem->setParentItem($this);
        }

        return $this;
    }

    public function removeChildrenItem(self $childrenItem): static
    {
        if ($this->childrenItems->removeElement($childrenItem)) {
            // set the owning side to null (unless already changed)
            if ($childrenItem->getParentItem() === $this) {
                $childrenItem->setParentItem(null);
            }
        }

        return $this;
    }

    public function getType(): ?WorkingItemType
    {
        return $this->type;
    }

    public function setType(?WorkingItemType $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): static
    {
        $this->link = $link;

        return $this;
    }

    public function getPlannedVersion(): ?Version
    {
        return $this->plannedVersion;
    }

    public function setPlannedVersion(?Version $plannedVersion): static
    {
        $this->plannedVersion = $plannedVersion;

        return $this;
    }

    public function getActualVersion(): ?Version
    {
        return $this->actualVersion;
    }

    public function setActualVersion(?Version $actualVersion): static
    {
        $this->actualVersion = $actualVersion;

        return $this;
    }

    /**
     * @return Collection<int, MergeRequest>
     */
    public function getMergeRequests(): Collection
    {
        return $this->mergeRequests;
    }

    public function addMergeRequest(MergeRequest $mergeRequest): static
    {
        if (!$this->mergeRequests->contains($mergeRequest)) {
            $this->mergeRequests->add($mergeRequest);
            $mergeRequest->addWorkingItem($this);
        }

        return $this;
    }

    public function removeMergeRequest(MergeRequest $mergeRequest): static
    {
        if ($this->mergeRequests->removeElement($mergeRequest)) {
            $mergeRequest->removeWorkingItem($this);
        }

        return $this;
    }

    public function getSupportBy(): ?Worker
    {
        return $this->supportBy;
    }

    public function setSupportBy(?Worker $supportBy): static
    {
        $this->supportBy = $supportBy;

        return $this;
    }

    public function getSupportedAt(): ?\DateTimeImmutable
    {
        return $this->supportedAt;
    }

    public function setSupportedAt(?\DateTimeImmutable $supportedAt): static
    {
        $this->supportedAt = $supportedAt;

        return $this;
    }

    public function getVersionedAt(): ?\DateTimeImmutable
    {
        return $this->versionedAt;
    }

    public function setVersionedAt(\DateTimeImmutable $versionedAt): static
    {
        $this->versionedAt = $versionedAt;

        return $this;
    }

    public function getPlannedAt(): ?\DateTimeImmutable
    {
        return $this->plannedAt;
    }

    public function setPlannedAt(\DateTimeImmutable $plannedAt): static
    {
        $this->plannedAt = $plannedAt;

        return $this;
    }

    public function getRealizedAt(): ?\DateTimeImmutable
    {
        return $this->realizedAt;
    }

    public function setRealizedAt(\DateTimeImmutable $realizedAt): static
    {
        $this->realizedAt = $realizedAt;

        return $this;
    }

    public function getRealizedBy(): ?Worker
    {
        return $this->realizedBy;
    }

    public function setRealizedBy(?Worker $realizedBy): static
    {
        $this->realizedBy = $realizedBy;

        return $this;
    }

    public function getEscalatedBy(): ?Worker
    {
        return $this->escalatedBy;
    }

    public function setEscalatedBy(?Worker $escalatedBy): static
    {
        $this->escalatedBy = $escalatedBy;

        return $this;
    }

    public function getEscalatedAt(): ?\DateTimeImmutable
    {
        return $this->escalatedAt;
    }

    public function setEscalatedAt(\DateTimeImmutable $escalatedAt): static
    {
        $this->escalatedAt = $escalatedAt;

        return $this;
    }

    public function getPriorityScore(): ?int
    {
        return $this->priorityScore;
    }

    public function setPriorityScore(?int $priorityScore): static
    {
        $this->priorityScore = $priorityScore;

        return $this;
    }
}
