<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\DTO\Version\ExistingVersionPreparedVersionDTO;
use App\DTO\Version\NewVersionPreparedVersionDTO;
use App\DTO\Version\PreparedVersionDTO;
use App\Repository\SubmoduleNewVersionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource]
#[ORM\Entity(SubmoduleNewVersionRepository::class)]
class SubmoduleNewVersion extends SubmoduleVersion
{
    #[ORM\ManyToOne()]
    #[ORM\JoinColumn(nullable: false)]
    private ?Version $submoduleVersion = null;

    public function getSubmoduleVersion(): ?Version
    {
        return $this->submoduleVersion;
    }

    public function setSubmoduleVersion(?Version $submoduleVersion): static
    {
        $this->submoduleVersion = $submoduleVersion;

        return $this;
    }

    public function createSubmodulepreparedVersion(): PreparedVersionDTO
    {
        $submoduleVersion = $this->submoduleVersion;
        $submoduleDevVersion = $submoduleVersion->getRepository()->getDevVersion();

        if ($submoduleDevVersion->getMergedMergeRequests()->count() === 0) {
            return new ExistingVersionPreparedVersionDTO(
                targetVersion: $submoduleVersion
            );
        }

        return new NewVersionPreparedVersionDTO(
            versionType: $submoduleVersion->getVersionType(),
            versionCode: '',
            releaseVersion: $submoduleVersion->isReleaseVersion(),
        );
    }
}
