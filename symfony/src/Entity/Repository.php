<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\Parameter;
use App\ApiResource\BasedCode;
use App\ApiResource\VersionTypeEnum;
use App\Controller\Repository\CreateNewReleaseController;
use App\Controller\Repository\PrepareVersionController;
use App\Controller\Repository\RepositorySearchBranchController;
use App\Controller\Repository\SyncRepositoryMergeRequestsController;
use App\DTO\Repository\Branch;
use App\DTO\Version\PreparedVersionDTO;
use App\Filter\Repository\RepositoryGlobalSearchFilter;
use App\Repository\RepositoryRepository;
use App\Validator\BasedCode\IsValidBasedCodeIdentifier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

#[
    ORM\Entity(repositoryClass: RepositoryRepository::class)]
#[ORM\UniqueConstraint('repository_unique_code', ['code', 'based_code_identifier'])]
#[ORM\UniqueConstraint('repository_unique_name', ['name', 'based_code_identifier'])]
#[UniqueEntity(fields: ['code', 'basedCodeIdentifier'], message: 'Ce dépôt est déjà importé')]
#[UniqueEntity(fields: ['name', 'basedCodeIdentifier'], message: 'Ce nom de dépôt est déjà utilisé')]
#[ApiResource(
    normalizationContext: ['groups' => self::API_READ_GROUP],
    denormalizationContext: ['groups' => self::API_WRITE_GROUP]
)]
#[Get]
#[GetCollection]
#[Post]
#[Patch]
#[Get(
    uriTemplate: '/repositories/{code}/merge_requests/sync',
    controller: SyncRepositoryMergeRequestsController::class,
    openapi: new Operation(
        responses: [Response::HTTP_NO_CONTENT]
    ),
    name: 'sync_repository_merge_requests'
)]
#[Get(
    uriTemplate: '/repositories/{code}/prepare_new_version',
    controller: PrepareVersionController::class,
    normalizationContext: ['groups' => PreparedVersionDTO::API_READ_GROUP],
    output: PreparedVersionDTO::class,
    name: 'prepare_new_version'
)]
#[Post(
    uriTemplate: '/repositories/{code}/new_version',
    controller: CreateNewReleaseController::class,
    denormalizationContext: ['groups' => PreparedVersionDTO::API_WRITE_GROUP],
    input: PreparedVersionDTO::class,
    name: 'create_new_version'
)]
#[ApiFilter(RepositoryGlobalSearchFilter::class, properties: ['global_search'])]
#[ORM\HasLifecycleCallbacks]
class Repository
{
    const string API_READ_GROUP = self::class.':read';
    const string API_WRITE_GROUP = self::class.':write';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[ApiProperty(identifier: true)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        Version::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    #[Assert\NotBlank(message: 'Le code doit être défini')]
    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: 'Le code doit contenir au moins {{ limit }} caractères',
        maxMessage: 'Le code doit contenir au maximum {{ limit }} caractères'
    )]
    private ?string $code = null;

    #[ORM\Column(length: 50)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        Version::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    #[Assert\NotBlank(message: 'Le nom doit être défini')]
    #[Assert\Length(
        min: 5,
        max: 50,
        minMessage: 'Le nom doit contenir au moins {{ limit }} caractères',
        maxMessage: 'Le nom doit contenir au maximum {{ limit }} caractères'
    )]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        Version::API_READ_GROUP,
    ])]
    #[Assert\NotBlank(message: 'L\'url doit être défini')]
    #[Assert\Length(
        max: 255,
        maxMessage: 'L\'url doit contenir au maximum {{ limit }} caractères'
    )]
    #[Assert\Url(message: 'L\url doit être valide')]
    private ?string $link = null;

    // TODO : encode before save
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups([
        BasedCode::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    #[Assert\NotBlank(message: 'Le token d\'accès doit être défini')]
    #[Assert\Length(
        min: 5,
        max: 255,
        minMessage: 'Le token doit contenir au moins {{ limit }} caractères',
        maxMessage: 'Le token doit contenir au maximum {{ limit }} caractères'
    )]
    private ?string $token = null;

    #[ORM\Column(length: 10)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
    ])]
    #[Assert\NotBlank(message: 'La branche principale doit être renseignée')]
    #[Assert\Length(
        min: 3,
        max: 10,
        minMessage: 'Le nom de la branche principale doit contenir au moins {{ limit }} caractères',
        maxMessage: 'Le nom de la branche principale doit contenir au maximum {{ limit }} caractères'
    )]
    private ?string $mainBranch = null;

    #[ORM\Column(length: 10)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
    ])]
    #[Assert\Length(
        max: 10,
        maxMessage: 'Le nom de la branche de développement doit contenir au maximum {{ limit }} caractères'
    )]
    #[Assert\Expression(
        expression: 'value !== this.getMainBranch()',
        message: 'La branche de dev doit être différente de celle de production'
    )]
    private ?string $devBranch = null;


    /**
     * @var Collection<int, Version>
     */
    #[ORM\OneToMany(targetEntity: Version::class, mappedBy: 'repository', cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[ORM\OrderBy(["versionedAt" => "DESC"])]
    #[Groups([self::API_READ_GROUP])]
    private Collection $versions;

    /**
     * @var Collection<int, Instance>
     */
    #[ORM\OneToMany(targetEntity: Instance::class, mappedBy: 'repository')]
    private Collection $instances;

    #[ORM\Column(length: 30)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
    ])]
    #[Assert\NotBlank(message: 'Le nom de domaine doit être renseigné')]
    #[Assert\Length(
        min: 5,
        max: 30,
        minMessage: 'Le nom de domaine doit contenir au moins {{ limit }} caractères',
        maxMessage: 'Le nom de domaine doit contenir au maximum {{ limit }} caractères'
    )]
    private ?string $host = null;

    #[ORM\Column(length: 10)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    #[Assert\NotBlank(message: 'L\'identifiant de la base de code doit être renseigné')]
    #[IsValidBasedCodeIdentifier]
    private ?string $basedCodeIdentifier = null;

    #[ORM\Column(options: ['default' => true])]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        BasedCode::API_READ_GROUP,
    ])]
    #[Assert\NotNull(message: 'Vous devez définir si l\'outil doit créer des branches pour chaque nouvelle version')]
    private bool $createBranchForNewVersion = true;

    /**
     * @var Collection<int, Submodule>
     */
    #[ORM\OneToMany(targetEntity: Submodule::class, mappedBy: 'parent', cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups([
        BasedCode::API_READ_GROUP,
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private Collection $submodules;

    /**
     * @var Collection<int, Submodule>
     */
    #[ORM\OneToMany(targetEntity: Submodule::class, mappedBy: 'child', cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[Groups([
        BasedCode::API_READ_GROUP,
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private Collection $parentRepositories;

    public function __construct()
    {
        $this->versions = new ArrayCollection();
        $this->instances = new ArrayCollection();
        $this->submodules = new ArrayCollection();
        $this->parentRepositories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function getLinkEncoded(): ?string
    {
        return urlencode($this->link ?? '');
    }

    public function setLink(string $link): static
    {
        $this->link = $link;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): static
    {
        $this->token = $token;

        return $this;
    }

    public function getMainBranch(): ?string
    {
        return $this->mainBranch;
    }

    public function setMainBranch(string $mainBranch): static
    {
        $this->mainBranch = $mainBranch;

        return $this;
    }

    public function getDevBranch(): ?string
    {
        return $this->devBranch;
    }

    public function setDevBranch(string $devBranch): static
    {
        $this->devBranch = $devBranch;

        return $this;
    }

    public function getSubmoduleWithCode(string $code): ?Submodule
    {
        foreach ($this->getSubModules() as $subModule) {
            if ($subModule->getChild()->getCode() === $code) {
                return $subModule;
            }
        }

        return null;
    }

    public function getSubmoduleWithLink(string $link): ?Submodule
    {
        foreach ($this->getSubModules() as $subModule) {
            if ($subModule->getChild()->getLink() === $link) {
                return $subModule;
            }
        }

        return null;
    }

    /**
     * @return Collection<int, Version>
     */
    public function getVersions(): Collection
    {
        return $this->versions;
    }

    public function addVersion(Version $version): static
    {
        if (!$this->versions->contains($version)) {
            $this->versions->add($version);
            $version->setRepository($this);
        }

        return $this;
    }

    public function removeVersion(Version $version): static
    {
        if ($this->versions->removeElement($version)) {
            // set the owning side to null (unless already changed)
            if ($version->getRepository() === $this) {
                $version->setRepository(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Instance>
     */
    public function getInstances(): Collection
    {
        return $this->instances;
    }

    public function addInstance(Instance $instance): static
    {
        if (!$this->instances->contains($instance)) {
            $this->instances->add($instance);
            $instance->setRepository($this);
        }

        return $this;
    }

    public function removeInstance(Instance $instance): static
    {
        if ($this->instances->removeElement($instance)) {
            // set the owning side to null (unless already changed)
            if ($instance->getRepository() === $this) {
                $instance->setRepository(null);
            }
        }

        return $this;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(string $host): static
    {
        $this->host = $host;

        return $this;
    }

    public function getBasedCodeIdentifier(): ?string
    {
        return $this->basedCodeIdentifier;
    }

    public function setBasedCodeIdentifier(string $basedCodeIdentifier): static
    {
        $this->basedCodeIdentifier = $basedCodeIdentifier;

        return $this;
    }

    #[Groups([
        self::API_READ_GROUP,
    ])]
    public function getLatestVersion(): ?Version
    {
        foreach ($this->versions as $version) {
            if ($version->isVersioned()) {
                return $version;
            }
        }

        return null;
    }

    public function getDevVersion(): ?Version
    {
        foreach ($this->versions as $version) {
            if (!$version->isVersioned()) {
                return $version;
            }
        }

        return $this->createDevVersion();
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function initVersion(): void
    {
        if (null !== $this->getDevVersion()) {
            return;
        }

        $devVersion = $this->createDevVersion();
        $this->addVersion($devVersion);
    }

    public function createDevVersion(): Version
    {
        $version = (new Version())
            ->setRepository($this)
            ->setVersionType(VersionTypeEnum::PATCH)
            ->setReleaseVersion(false)
            ->setCode('dev');

        $this->addVersion($version);
        return $version;
    }

    #[Groups([
        BasedCode::API_READ_GROUP,
        self::API_READ_GROUP,
    ])]
    public function getIsAlreadyImported(): bool
    {
        return null !== $this->id;
    }

    public function isCreateBranchForNewVersion(): ?bool
    {
        return $this->createBranchForNewVersion;
    }

    public function setCreateBranchForNewVersion(bool $createBranchForNewVersion): static
    {
        $this->createBranchForNewVersion = $createBranchForNewVersion;

        return $this;
    }

    public function isBranchUsedToVersion(string $branch): bool
    {
        return $branch === $this->getDevBranch() || $branch === $this->getMainBranch();
    }

    #[Groups([
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    public function getLatestVersionCode(): ?string
    {
        return $this->getLatestVersion()?->getCode();
    }

    /**
     * @return Collection<int, Submodule>
     */
    public function getSubmodules(): Collection
    {
        return $this->submodules;
    }

    public function addSubmodule(Submodule $submodule): static
    {
        if (!$this->submodules->contains($submodule)) {
            $this->submodules->add($submodule);
            $submodule->setParent($this);
        }

        return $this;
    }

    public function removeSubmodule(Submodule $submodule): static
    {
        if ($this->submodules->removeElement($submodule)) {
            // set the owning side to null (unless already changed)
            if ($submodule->getParent() === $this) {
                $submodule->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Submodule>
     */
    public function getParentRepositories(): Collection
    {
        return $this->parentRepositories;
    }

    public function addParentRepository(Submodule $parentRepository): static
    {
        if (!$this->parentRepositories->contains($parentRepository)) {
            $this->parentRepositories->add($parentRepository);
            $parentRepository->setChild($this);
        }

        return $this;
    }

    public function removeParentRepository(Submodule $parentRepository): static
    {
        if ($this->parentRepositories->removeElement($parentRepository)) {
            // set the owning side to null (unless already changed)
            if ($parentRepository->getChild() === $this) {
                $parentRepository->setChild(null);
            }
        }

        return $this;
    }
}
