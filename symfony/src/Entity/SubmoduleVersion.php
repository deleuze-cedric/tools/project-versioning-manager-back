<?php

namespace App\Entity;

use App\ApiResource\UpdateTypeEnum;
use App\DTO\Version\PreparedVersionDTO;
use App\Repository\SubmoduleNewVersionRepository;
use App\Repository\SubmoduleVersionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SubmoduleVersionRepository::class)]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'update_type', type: 'string')]
#[ORM\DiscriminatorMap([
    UpdateTypeEnum::BRANCH->value => SubmoduleBranchVersion::class,
    UpdateTypeEnum::EXISTING_VERSION->value => SubmoduleExistingVersion::class,
    UpdateTypeEnum::NEW_VERSION->value => SubmoduleNewVersion::class,
])]
abstract class SubmoduleVersion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'submoduleVersions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Version $parentVersion = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Repository $submodule = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParentVersion(): ?Version
    {
        return $this->parentVersion;
    }

    public function setParentVersion(?Version $parentVersion): static
    {
        $this->parentVersion = $parentVersion;

        return $this;
    }

    public function getSubmodule(): ?Repository
    {
        return $this->submodule;
    }

    public function setSubmodule(?Repository $submodule): static
    {
        $this->submodule = $submodule;

        return $this;
    }

    public abstract function createSubmodulepreparedVersion(): PreparedVersionDTO;
}
