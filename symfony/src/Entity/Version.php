<?php
declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\ApiResource\VersionTypeEnum;
use App\DTO\Version\PreparedVersionDTO;
use App\Filter\Version\RepositoryReleasedVersionFilter;
use App\Filter\Version\VersionGlobalSearchFilter;
use App\Filter\Version\VersionNotInMergeRequestDependenciesFilter;
use App\Repository\VersionRepository;
use App\Service\Version\VersionBuilder\NewVersionReleaser;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Serializer\Attribute\SerializedName;
use Symfony\Component\Uid\Uuid;
use Throwable;

#[ORM\Entity(repositoryClass: VersionRepository::class)]
#[ORM\UniqueConstraint('version_unique_code_per_repository', ['code', 'repository_id'])]
#[ApiFilter(VersionGlobalSearchFilter::class, properties: ['search'])]
#[ApiFilter(VersionNotInMergeRequestDependenciesFilter::class, properties: ['excludes_merge_request_dependencies'])]
#[ApiFilter(RepositoryReleasedVersionFilter::class, properties: ['repository_released_version'])]
#[ApiResource(
    normalizationContext: [
        'groups' => [self::API_READ_GROUP],
    ],
    denormalizationContext: [
        'groups' => [self::API_WRITE_GROUP],
    ],
)]
class Version
{
    const string API_READ_GROUP = self::class.':read';
    const string API_WRITE_GROUP = self::class.':write';

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups([
        self::API_READ_GROUP,
        Repository::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    private ?Uuid $id = null;

    #[ORM\Column(length: 10, nullable: true)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        Repository::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    private ?string $code = null;

    #[ORM\ManyToOne(inversedBy: 'versions')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        MergeRequest::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    private ?Repository $repository = null;

    /**
     * @var Collection<int, PatchNoteSection>
     */
    #[ORM\OneToMany(targetEntity: PatchNoteSection::class, mappedBy: 'version')]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        Repository::API_READ_GROUP,
    ])]
    private Collection $additionalPatchNoteSections;

    /**
     * @var Collection<int, MergeRequest>
     */
    #[ORM\OneToMany(targetEntity: MergeRequest::class, mappedBy: 'version')]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private Collection $mergeRequests;

    /**
     * @var Collection<int, Commit>
     */
    #[ORM\OneToMany(targetEntity: Commit::class, mappedBy: 'version', orphanRemoval: true)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private Collection $commits;

    /**
     * @var Collection<int, MergeRequest>
     */
    #[ORM\ManyToMany(targetEntity: MergeRequest::class, mappedBy: 'versionDependencies')]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private Collection $requiredByMergeRequests;

    /**
     * @var Collection<int, Instance>
     */
    #[ORM\ManyToMany(targetEntity: Instance::class, mappedBy: 'history')]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
    ])]
    private Collection $instances;

    #[ORM\Column(nullable: true)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        Repository::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    private ?DateTimeImmutable $versionedAt = null;

    /**
     * @var Collection<int, SubmoduleVersion>
     */
    #[ORM\OneToMany(targetEntity: SubmoduleVersion::class, mappedBy: 'parentVersion', orphanRemoval: true)]
    private Collection $submoduleVersions;

    #[ORM\Column(enumType: VersionTypeEnum::class)]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        Repository::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    private ?VersionTypeEnum $versionType = null;

    #[ORM\Column]
    #[Groups([
        self::API_READ_GROUP,
        self::API_WRITE_GROUP,
        Repository::API_READ_GROUP,
        MergeRequest::API_READ_GROUP,
        PreparedVersionDTO::API_READ_GROUP,
    ])]
    private ?bool $releaseVersion = null;

    /**
     * @var Collection<int, WorkingItem>
     */
    #[ORM\OneToMany(targetEntity: WorkingItem::class, mappedBy: 'plannedVersion')]
    private Collection $plannedWorkingItems;

    /**
     * @var Collection<int, WorkingItem>
     */
    #[ORM\OneToMany(targetEntity: WorkingItem::class, mappedBy: 'actualVersion')]
    private Collection $actualWorkingItems;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $commitSha = null;

    public function __construct()
    {
        $this->additionalPatchNoteSections = new ArrayCollection();
        $this->mergeRequests = new ArrayCollection();
        $this->commits = new ArrayCollection();
        $this->requiredByMergeRequests = new ArrayCollection();
        $this->instances = new ArrayCollection();
        $this->submoduleVersions = new ArrayCollection();
        $this->plannedWorkingItems = new ArrayCollection();
        $this->actualWorkingItems = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): static
    {
        $this->code = $code;

        return $this;
    }

    public function getRepository(): ?Repository
    {
        return $this->repository;
    }

    public function setRepository(?Repository $repository): static
    {
        $this->repository = $repository;

        return $this;
    }

    /**
     * @return Collection<int, PatchNoteSection>
     */
    public function getAdditionalPatchNoteSections(): Collection
    {
        return $this->additionalPatchNoteSections;
    }

    public function addAdditionalPatchNoteSection(PatchNoteSection $patchNoteSection): static
    {
        if (!$this->additionalPatchNoteSections->contains($patchNoteSection)) {
            $this->additionalPatchNoteSections->add($patchNoteSection);
            $patchNoteSection->setVersion($this);
        }

        return $this;
    }

    public function removeAdditionalPatchNoteSection(PatchNoteSection $patchNoteSection): static
    {
        if ($this->additionalPatchNoteSections->removeElement($patchNoteSection)) {
            // set the owning side to null (unless already changed)
            if ($patchNoteSection->getVersion() === $this) {
                $patchNoteSection->setVersion(null);
            }
        }

        return $this;
    }

    /**
     * @return array<PatchNoteSection>
     */
    #[SerializedName('content')]
    #[Groups([
        self::API_READ_GROUP,
        Repository::API_READ_GROUP,
    ])]
    public function getAllPatchNoteSections(): array
    {
        /** @var array<PatchNoteSection> $patchNoteSections */
        $patchNoteSections = $this->additionalPatchNoteSections->toArray();

        foreach ($this->commits as $commit) {
            $patchNoteSections = [
                ...$patchNoteSections,
                ...$commit->getPatchNoteSections(),
            ];
        }

        foreach ($this->mergeRequests as $mergeRequest) {
            $patchNoteSections = [
                ...$patchNoteSections,
                ...$mergeRequest->getPatchNoteSections(),
            ];
        }

        /** @var array<PatchNoteSection> $groupedPatchNoteSections */
        $groupedPatchNoteSections = [];

        foreach ($patchNoteSections as $patchNoteSection) {
            $code = $patchNoteSection->getCode();

            if (!isset($groupedPatchNoteSections[$code])) {
                $groupedPatchNoteSections[$code] = $patchNoteSection;
                continue;
            }

            foreach ($patchNoteSection->getContent() as $content) {
                $groupedPatchNoteSections[$code]->addContentLine($content);
            }
        }

        // It's a little bit more complicated
        // We need to diff the versioned date of the previous version and merge all patch notes from this one to new one
        // We need to display features/fix removing when it's a rollback version
        foreach ($this->submoduleVersions as $submoduleVersion) {
            if(!$submoduleVersion instanceof SubmoduleNewVersion){
                continue;
            }

            $submoduleVersionGroupedPatchNoteSections = $submoduleVersion->getSubmoduleVersion()->getAllPatchNoteSections();

            foreach ($submoduleVersionGroupedPatchNoteSections as $patchNoteSection) {
                $code = $patchNoteSection->getCode();

                if (!isset($groupedPatchNoteSections[$code])) {
                    $groupedPatchNoteSections[$code] = $patchNoteSection;
                    continue;
                }

                foreach ($patchNoteSection->getContent() as $content) {
                    $groupedPatchNoteSections[$code]->addContentLine($content);
                }
            }
        }

        return $groupedPatchNoteSections;
    }

    /**
     * @return Collection<int, MergeRequest>
     */
    public function getMergeRequests(): Collection
    {
        return $this->mergeRequests;
    }

    /**
     * @return Collection<int, MergeRequest>
     */
    public function getMergedMergeRequests(): Collection
    {
        return $this->mergeRequests->filter(function (MergeRequest $mergeRequest) {
            return $mergeRequest->isMerged();
        });
    }

    public function addMergeRequest(MergeRequest $mergeRequest): static
    {
        if (!$this->mergeRequests->contains($mergeRequest)) {
            $this->mergeRequests->add($mergeRequest);
            $mergeRequest->setVersion($this);
        }

        return $this;
    }

    public function removeMergeRequest(MergeRequest $mergeRequest): static
    {
        if ($this->mergeRequests->removeElement($mergeRequest)) {
            // set the owning side to null (unless already changed)
            if ($mergeRequest->getVersion() === $this) {
                $mergeRequest->setVersion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Commit>
     */
    public function getCommits(): Collection
    {
        return $this->commits;
    }

    public function addCommit(Commit $commit): static
    {
        if (!$this->commits->contains($commit)) {
            $this->commits->add($commit);
            $commit->setVersion($this);
        }

        return $this;
    }

    public function removeCommit(Commit $commit): static
    {
        if ($this->commits->removeElement($commit)) {
            // set the owning side to null (unless already changed)
            if ($commit->getVersion() === $this) {
                $commit->setVersion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, MergeRequest>
     */
    public function getRequiredByMergeRequests(): Collection
    {
        return $this->requiredByMergeRequests;
    }

    public function addRequiredByMergeRequest(MergeRequest $requiredByMergeRequest): static
    {
        if (!$this->requiredByMergeRequests->contains($requiredByMergeRequest)) {
            $this->requiredByMergeRequests->add($requiredByMergeRequest);
            $requiredByMergeRequest->addVersionDependency($this);
        }

        return $this;
    }

    public function removeRequiredByMergeRequest(MergeRequest $requiredByMergeRequest): static
    {
        if ($this->requiredByMergeRequests->removeElement($requiredByMergeRequest)) {
            $requiredByMergeRequest->removeVersionDependency($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Instance>
     */
    public function getInstances(): Collection
    {
        return $this->instances;
    }

    public function addInstance(Instance $instance): static
    {
        if (!$this->instances->contains($instance)) {
            $this->instances->add($instance);
            $instance->addHistory($this);
        }

        return $this;
    }

    public function removeInstance(Instance $instance): static
    {
        if ($this->instances->removeElement($instance)) {
            $instance->removeHistory($this);
        }

        return $this;
    }

    public function isVersioned(): bool
    {
        return null !== $this->versionedAt;
    }

    public function getVersionedAt(): ?DateTimeImmutable
    {
        return $this->versionedAt;
    }

    public function setVersionedAt(?DateTimeImmutable $versionedAt): static
    {
        $this->versionedAt = $versionedAt;

        return $this;
    }

    /**
     * @return Collection<MergeRequest>
     */
    public function getDevMergeRequests(): Collection
    {
        return $this->getMergeRequests()->filter(function (mergeRequest $mergeRequest) {
            return $mergeRequest->isDevMergeRequest();
        });
    }

    /**
     * @return Collection<MergeRequest>
     */
    public function getMainMergeRequests(): Collection
    {
        return $this->getMergeRequests()->filter(function (mergeRequest $mergeRequest) {
            return $mergeRequest->isMainMergeRequest();
        });
    }

    /**
     * @return Collection<int, SubmoduleVersion>
     */
    public function getSubmoduleVersions(): Collection
    {
        return $this->submoduleVersions;
    }

    public function addSubmoduleVersion(SubmoduleVersion $submoduleVersion): static
    {
        if (!$this->submoduleVersions->contains($submoduleVersion)) {
            $this->submoduleVersions->add($submoduleVersion);
            $submoduleVersion->setParentVersion($this);
        }

        return $this;
    }

    public function removeSubmoduleVersion(SubmoduleVersion $submoduleVersion): static
    {
        if ($this->submoduleVersions->removeElement($submoduleVersion)) {
            // set the owning side to null (unless already changed)
            if ($submoduleVersion->getParentVersion() === $this) {
                $submoduleVersion->setParentVersion(null);
            }
        }

        return $this;
    }

    public function getVersionType(): ?VersionTypeEnum
    {
        return $this->versionType;
    }

    public function setVersionType(VersionTypeEnum $versionType): static
    {
        $this->versionType = $versionType;

        return $this;
    }

    public function isReleaseVersion(): ?bool
    {
        return $this->releaseVersion;
    }

    public function setReleaseVersion(bool $releaseVersion): static
    {
        $this->releaseVersion = $releaseVersion;

        return $this;
    }

    public function getVersionForSubmodule(Repository $submodule): ?SubmoduleVersion
    {
        $submoduleID = $submodule->getId();

        foreach ($this->getSubmoduleVersions() as $submoduleVersion) {
            if ($submoduleID === $submoduleVersion->getSubmodule()->getId()) {
                return $submoduleVersion;
            }
        }

        return null;
    }

    /**
     * @return Collection<int, WorkingItem>
     */
    public function getPlannedWorkingItems(): Collection
    {
        return $this->plannedWorkingItems;
    }

    public function addPlannedWorkingItem(WorkingItem $plannedWorkingItem): static
    {
        if (!$this->plannedWorkingItems->contains($plannedWorkingItem)) {
            $this->plannedWorkingItems->add($plannedWorkingItem);
            $plannedWorkingItem->setPlannedVersion($this);
        }

        return $this;
    }

    public function removePlannedWorkingItem(WorkingItem $plannedWorkingItem): static
    {
        if ($this->plannedWorkingItems->removeElement($plannedWorkingItem)) {
            // set the owning side to null (unless already changed)
            if ($plannedWorkingItem->getPlannedVersion() === $this) {
                $plannedWorkingItem->setPlannedVersion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, WorkingItem>
     */
    public function getActualWorkingItems(): Collection
    {
        return $this->actualWorkingItems;
    }

    public function addActualWorkingItem(WorkingItem $actualWorkingItem): static
    {
        if (!$this->actualWorkingItems->contains($actualWorkingItem)) {
            $this->actualWorkingItems->add($actualWorkingItem);
            $actualWorkingItem->setActualVersion($this);
        }

        return $this;
    }

    public function removeActualWorkingItem(WorkingItem $actualWorkingItem): static
    {
        if ($this->actualWorkingItems->removeElement($actualWorkingItem)) {
            // set the owning side to null (unless already changed)
            if ($actualWorkingItem->getActualVersion() === $this) {
                $actualWorkingItem->setActualVersion(null);
            }
        }

        return $this;
    }

    /**
     * @return array{
     *     MAJOR: int,
     *     MINOR: int,
     *     PATCH: int,
     * }
     */
    public function getDecomposedCode(): array
    {
        $code = $this->getCode();

        $decomposedCode = explode('.', $code);

        if (count($decomposedCode) !== 3) {
            return [];
        }

        try {
            return [
                'MAJOR' => (int)$decomposedCode[0],
                'MINOR' => (int)$decomposedCode[1],
                'PATCH' => (int)$decomposedCode[2],
            ];
        } catch (Throwable $e) {
            return [];
        }
    }

    public function getCommitSha(): ?string
    {
        return $this->commitSha;
    }

    public function setCommitSha(string $commitSha): static
    {
        $this->commitSha = $commitSha;

        return $this;
    }
}
