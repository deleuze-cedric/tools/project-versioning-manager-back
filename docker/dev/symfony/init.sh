#!/bin/bash

git config --global user.email $GIT_EMAIL;
git config --global user.name $GIT_NAME;

cd /var/www/html

if [ ! -f ./composer.json ]; then
    symfony new symfony;
    mv symfony/* ./;
    mv symfony/.* ./;
    rm -rf .git;
    chown -R $USER_ID:$GROUP_ID ./;
    composer require symfony/apache-pack
fi

exec /usr/local/bin/apache2-foreground