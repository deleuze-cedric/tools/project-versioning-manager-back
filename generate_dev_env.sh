#!/bin/bash

echo "# Configuration utilisateur" > .env
echo "USER_ID=$(id -u $USER)" >> .env
echo "GROUP_ID=$(id -g $USER)" >> .env
echo "" >> .env

echo "# Configuration git" >> .env
echo "GIT_EMAIL=$(git config user.email)" >> .env
echo "GIT_NAME=\"$(git config user.name)\"" >> .env
echo "" >> .env

echo "# Docker" >> .env
echo "PROJECT_NAME=api-project-versioning-manager" >> .env
echo "PROJECT_DOMAIN=api.project-versioning-manager" >> .env
echo "DOMAIN=local.com" >> .env
echo "" >> .env

echo "# Postgres" >> .env
echo "POSTGRES_PASSWORD=root" >> .env
echo "POSTGRES_USER=root" >> .env
echo "POSTGRES_EMAIL_USER=$(git config user.email)" >> .env

echo "The .env file has been successfully generated"